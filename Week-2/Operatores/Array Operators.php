<!-- Array operators :- +(simply $x+$y) ,==(true if both $x and $y have the same key/valye pair) ,===(true if both $x                  and  $y have the same key/valye pairs in the same order and same type) ,
                    !=(true if $x is not equal to $y) ,<>(true if $x is not equal to $y) ,!==(true if $x is  is not identical to $y) -->


<!--Array Operators-->

<!DOCTYPE html>
<html>
<body>

<?php
$x = array("a" => "hey", "b" => " there");  
$y = array("c" => " i'am", "d" => " nikhil"); 
echo "<b><P>Array operators</b><p>";
echo"<p>Union Operators <b>x + y</b></p>"; 
print_r($x + $y); 
// union of $x and $y
//Kind of addition of two arrays


$x = array("a" => "hey", "b" => " there");  
$y = array("c" => " i'am", "d" => " nikhil"); 
echo"<p>Equality Operators <b>x == y</b></p>"; 
var_dump($x==$y);
// Returns true if $x and $y have the same key/value pairs
//here we have diff values and key


$x = array("a" => "hey", "b" => " there");  
$y = array("c" => " i'am", "d" => " nikhil"); 
echo"<p>Identity Operators <b>x === y</b></p>"; 
var_dump($x===$y);
// Returns true if $x and $y have the same key/value pairs in the same order and of the same types
//here we have diff values and key


$x = array("a" => "hey", "b" => " there");  
$y = array("c" => " i'am", "d" => " nikhil"); 
echo"<p>Inequality Operators <b>x != y</b></p>"; 
var_dump($x!=$y);
// Returns true if $x is not equal to $y
//here $x is not equal to $y


$x = array("a" => "hey", "b" => " there");  
$y = array("c" => " i'am", "d" => " nikhil"); 
echo"<p>Inequality Operators <b>x <> y</b></p>"; 
var_dump($x<>$y);
// Returns true if $x is not equal to $y
//here $x is not equal to $y


$x = array("a" => "hey", "b" => " there");  
$y = array("c" => " i'am", "d" => " nikhil"); 
echo"<p>Non Identity Operators <b>x !== y</b></p>"; 
var_dump($x!==$y);
// Returns true if $x is not identical to $y
//here $x and $y has diff types

?>  

</body>
</html>
