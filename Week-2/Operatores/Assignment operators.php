<!-- 
Assignment operators :- =,+=,-+,*=,/=,%= -->

<!-- Assignment operators -->

<!doctype html>
<html>
<body>
    <?php 
    $x = 5;
    echo "<b><P>Assignment operators</b><p>";
    echo $x;

    $x = 5;
    $x += 7;
    echo "<p>Addition :- <b>x+=</b><p>"; 
    echo "<p>x=5 and x+=7<p>"; 
    echo $x;

    $x = 5;
    $x -= 7;
    echo "<p>Subtraction :- <b>x-=</b><p>";
    echo "<p>x=5 and x-=7<p>"; 
    echo $x;

    $x = 5;
    $x *= 7;
    echo "<p>Multiplication :- <b>x*=</b><p>";
    echo "<p>x=5 and x*=7<p>";  
    echo $x;

    $x = 5;
    $x /= 7;
    echo "<p>Division :- <b>x/=</b><p>";
    echo "<p>x=5 and x/=7<p>";  
    echo $x;

    $x = 5;
    $x %= 7;
    echo "<p>Modulus :- <b>x%=</b><p>";
    echo "<p>x=5 and x%=7<p>";  
    echo $x;

    ?>
</body>
<html>