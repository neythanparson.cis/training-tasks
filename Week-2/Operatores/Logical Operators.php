<!-- Logical operators :- and(true both $x and $y are true) ,or(true if either $x or $y is true) ,xor(true if either $x or $y is true but not both)
                     &&(true if $x and $y are true) ,||(true if either $x or $y is true),!(true if $x is not true) -->

<!--Logical operators-->


<!doctype html>
<html>
<body>

<?php
$x = 5;  
$y = 7;
if ($x == 5 and $y == 7)
echo "<b><P>Logical operators</b><p>";
echo"<p>And Operators <b>x == 5 and y == 7</b></p>";
// if both are true gives return value 
{
    echo "Hello world!";
}

$x = 5;  
$y = 7;
if ($x == 5 or $y == 55)
echo"<p>or Operators <b>x == 5 and y == 55</b></p>";
// if one of them is true gives return value 
// here, value of x is true but value of y is false 
{
    echo "Hello world!";
}

$x = 5;  
$y = 7;
if ($x == 83 or $y ==7)
echo"<p>xor Operators <b>x == 83 and y ==    5</b></p>";
// True if either $x or $y is true, but not both
// here, value of y is true but value of x is false 
{
    echo "Hello world!";
}

$x = 5;  
$y = 7;
if ($x == 5 or $y ==7)
echo"<p>And (&) Operators <b>x == 5 and y == 7</b></p>";
//True if both $x and $y are true	
{
    echo "Hello world!";
}

$x = 5;  
$y = 7;
if ($x == 5 or $y ==57)
echo"<p>Or (||) Operators <b>x == 5 and y == 57</b></p>";
//	True if either $x or $y is true
// here, value of x is true but value of y is false 

$x = 5;  
if ($x !== 99)
echo"<p>Not (!==) Operators <b>x!==99</b></p>";
//	True if either $x is not true

{
    echo "Hello world!";
}
?>  