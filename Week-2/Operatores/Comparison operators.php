<!-- 

Comparison operators :- ==,===(equal and same type),!= and <>(not equal),!==(not equal and not same type),> ,< ,>= ,<= ,<=>
 -->

<!-- Comparison operators -->

<!doctype html>
<html>
<body>
    <?php 
    $x = 5;
    $y = 7;
    echo "<b><P>Comparison operators</b><p>";
    echo "<p>Equal <b>x == y</b><p>"; 
    var_dump($x == $y); // returns true because values are equal
    
    $x = 5;
    $y = 7;
    echo "<p>identical :- <b>x === y</b><p>"; 
    var_dump($x === $y); // returns true because values are equal

    $x = 5;
    $y = 7;
    echo "<p>Not equal :- <b>x != y</b><p>"; 
    var_dump($x != $y); // returns true because values are equal

    $x = 5;
    $y = 7;
    echo "<p>Not Equal :- <b>x <> y</b><p>"; 
    var_dump($x <> $y); // returns true because values are equal

    $x = 5;
    $y = 7;
    echo "<p>Not Identical :- <b>x !== y</b><p>"; 
    var_dump($x !== $y); // returns true because values are equal

    $x = 5;
    $y = 7;
    echo "<p>Greater than :- <b>x > y</b><p>"; 
    var_dump($x > $y); // returns true because values are equal

    $x = 5;
    $y = 7;
    echo "<p>Less than :- <b>x < y</b><p>"; 
    var_dump($x < $y); // returns true because values are equal

    $x = 5;
    $y = 7;
    echo "<p>Greater than or equal to :- <b>x >= y</b><p>"; 
    var_dump($x >= $y); // returns true because values are equal

    $x = 5;
    $y = 7;
    echo "<p>Less than or equal to :- <b>x <= y</b><p>"; 
    var_dump($x <= $y); // returns true because values are equal

    // <=>	Spaceship (doubt)
    ?>
</body>
<html>