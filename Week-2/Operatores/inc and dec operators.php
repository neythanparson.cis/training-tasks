<!-- 

Increment/Decrement operators :- ++$x(Increments $x by one, then returns $x) ,$x++(Returns $x, then increments $x by one)
                                 --$x(Decrements $x by one, then returns $x) ,$x--(Returns $x, then decrements $x by one)
-->
<!-- PHP Increment / Decrement Operators -->

<!doctype html>
<html>
<body>
    <?php 
    $x = 5;
    echo "<b><P>Increment / Decrement operators</b><p>";
    echo "<p>Pre Increment :- <b>++x</b><p>"; 
    echo ++$x;

    $x = 5;
    echo "<p>Post Increment:- <b>x++</b><p>"; 
    echo $x++;

    $x = 5;
    echo "<p>Pre Decrement :- <b>--x</b><p>"; 
    echo --$x;

    $x = 5;
    echo "<p>Post Decrement :- <b>x--</b><p>"; 
    echo $x--;
    ?>
    </body>
    <html>