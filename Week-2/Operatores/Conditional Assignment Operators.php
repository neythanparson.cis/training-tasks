<!-- Conditional Assignment Operators -->

<!-- ?:	 Ternary -->

<?php
   // if empty($user) = TRUE, set $status = "anonymous"
   echo $status = (empty($user)) ? "Empty" : "Not empty (Welcome)";
   echo("<br>");

   $user = "John Doe"; // bcz user has some value.
   // if empty($user) = FALSE, set $status = "logged in"
   echo $status = (empty($user)) ? "Empty" : "Not empty (Welcome)";
   echo("<br>");
?>  



<!-- ??	 Null  coalescing -->

<?php
   // variable $user is the value of $_GET['user']
   // and 'anonymous' if it does not exist
   echo $user = $_GET["user"] ?? "anonymous";
   echo("<br>");
  
   // variable $color is "red" if $color does not exist or is null
   echo $color = $color ?? "red";
   
// var_dump ($user);
?>  


