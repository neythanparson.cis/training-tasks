<!-- array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40") in
a) ascending order sort by value
b) ascending order sort by Key
c) descending order sorting by Value
d) descending order sorting by Key -->

<?php
// <Q-1>
echo "<b> Ascending order by value </b>";
$arr1=["Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40"];

echo "<pre>";
print_r($arr1);
asort($arr1);

foreach($arr1 as $y => $y_value)
    {
        echo "Age of" ." ". $y . "is :" . $y_value .  "<br>";
    }
echo "<br>";
echo "<br>";
echo "<br>";

// <Q-2>
echo "<b> Ascending order by keys</b>";
$arr2=["Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40"];

echo "<pre>";
print_r($arr2);
ksort($arr2);

foreach($arr2 as $x => $x_value)
    {
        echo "Age of" ." ". $x . "is :" . $x_value .  "<br>";
    }
    echo "<br>";
    echo "<br>";
    echo "<br>";

// <Q-3>
echo "<b> Dscending order by value</b>";
$arr3=["Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40"];

echo "<pre>";
print_r($arr3);
arsort($arr3);

foreach($arr3 as $x => $x_value)
    {
        echo "Age of" ." ". $x . "is :" . $x_value .  "<br>";
    }
    echo "<br>";
    echo "<br>";
    echo "<br>";

// <Q-4>
echo "<b> Dscending order by keys</b>";
$arr4=["Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40"];

echo "<pre>";
print_r($arr4);
krsort($arr4);

foreach($arr4 as $x => $x_value)
    {
        echo "Age of" ." ". $x . "is :" . $x_value .  "<br>";
    }
    echo "<br>";
    echo "<br>";
    echo "<br>";

?>

