<!-- 
PROBLEM : From the below arrays, how would you extract the value 3?

$data1 = [0, 1, 2, 3, 4];

$data2 = ["zero" => 0, "one" => 1, "two" => 2, "three" => 3, "four" => 4];

$data3 = [
  [0,1],[2, [3] ]
];

$data4 =  [
    "a" => ["b" => 0, "c" => 1],
    "b" => ["e" => 2,"o" => ["b" => 3] ]
  ]; -->

<?php
echo "<p><h1>TASK-1</h1><p>";

$data1 = [0,1,2,3,4,];
$data2 = ["zero" => 0, "one" => 1, "two" => 2, "three" => 3, "four" => 4];
$data3 = [
  [0,1],[2, [3] ]
];
$data4 =  [
    "a" => ["b" => 0, "c" => 1],
    "b" => ["e" => 2,"o" => ["b" => 3] ]
  ];
  
  echo "<pre>";
  echo"<p><b>Input-1</b></p>";
  print_r ($data1);
  
  echo"<p><b>Output-1</b></p>";
  echo "Extracted value '3' from data1 is" . " " . $data1[3] . ".";
  echo"<br>";
 

  echo"<p><b>Input-2</b></p>";
  print_r ($data2);

  echo"<p><b>Output-2</b></p>";
  echo "Extracted value '3' from data2 is" . " " . $data2["three"] . ".";
  echo"<br>";
 
  
  echo"<p><b>Input-3</b></p>";
  print_r ($data3);

  echo"<p><b>Output-3</b></p>";
  echo "Extracted value '3' from data3 is" . " " . $data3[1][1][0] . ".";
  echo"<br>";
  

  echo"<p><b>Input-4</b></p>";
  print_r ($data4);

  echo"<p><b>Output-4</b></p>";
  echo "Extracted value '3' from data4 is" . " " . $data4["b"]["o"]["b"] . ".";
  echo"<br>";
?>
