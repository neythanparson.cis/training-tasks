<!-- 
PROBLEM : Create a new array with each comma-separated value as its own array element from the below string. Extra spacing should not be there for any element of the new array.

$string1 = "a,b,c,d,e,f";
$string2 = "0,,,,,,,0,1";
$string3 = "1, 23,4,5 6, 7 , 89";
$string4 = "987987987987987";
 -->

<?php

 echo "<p><h1>TASK-2</h1><p>";

$string1 = "a,b,c,d,e,f";
$string2 = "0,,,,,,,0,1";
$string3 = "1, 23,4,5 6, 7 , 89";
$string4 = "987987987987987";

echo"<pre>";
// String-1 to Array
echo"<b><p>=> Input-1</p></b>";                //Title Input
echo $string1;                              //Print String
echo"<b><p>Output-1</p></b>";               //Title Output
$new_str1 = str_replace(',', '', $string1); //Removes the space 
$arr1 = str_split($new_str1);               //Convert String To array
print_r($arr1);                             //Print Array
echo "<br>";                              
echo "<br>";                              
echo "<br>";                              

// String-2 to Array
echo"<b><p>=> Input-2</p></b>";                //Title Input
echo $string2;                              //Print String
echo"<b><p>Output-2</p></b>";               //Title Output
$new_str2 = str_replace(',', '', $string2); //Removes the space and " , "
$arr2 = str_split($new_str2);               //Convert String To array
print_r($arr2);                             //Print Array
echo "<br>";                              
echo "<br>";                              
echo "<br>";  

// String-3 to Array
echo"<b><p>=> Input-3</p></b>";                //Title Input
echo $string3;                              //Print String
echo"<b><p>Output-3</p></b>";               //Title Output
$new_str3 = str_replace(' ', '', $string3); //Removes the space 
$arr3 = (explode(",",$new_str3));           //explode -> to remove ","
print_r($arr3);                             //Print Array
echo "<br>";                              
echo "<br>";                              
echo "<br>";  

// String-4 to Array
echo"<b><p>=> Input-4</p></b>";                //Title Input
echo $string4;                              //Print String
echo"<b><p>Output-4</p></b>";               //Title Output
$arr4 = str_split($string4,3);              //Convert String To array
print_r ($arr4);                            //Print Array

?>
