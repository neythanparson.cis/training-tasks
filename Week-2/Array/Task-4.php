<!--
PROBLEM : You have two arrays like the following. One contains field labels, the other contains field values. Write a program to output the third array.

// INPUT 1
$keys = [
  "field1" => "first",
  "field2" => "second",
  "field3" => "third"
];

// INPUT 2
$values = [
  "field1value" => "dinosaur",
  "field2value" => "pig",
  "field3value" => "platypus"
];

// OUTPUT
$output = [
  "first" => "dinosaur",
  "second" => "pig",
  "third" => "platypus"
]; -->


<?php
echo "<b><h1>Task-4</h1></b>"; 
    $keys = [
            "field1" => "first",
            "field2" => "second",
            "field3" => "third"
            // values of $keys will be keys of output
    ];
	
    $values = [
            "field1value" => "Nikhil",
            "field2value" => "Het",
            "field3value" => "Krunal"
            // values of $values will be values of output
    ];
    
    echo "<pre>";
    echo "<p><h3>Input</h3></P>";
print_r($keys);//Print $keys
print_r($values);//Print $values
    
    $newArr=[];//Create New array
    $n=1;
foreach($keys as $k => $val){
//    print_r($k);
    $newArr[$val]["Name"]=$values[$k."value"];    
    $newArr[$val]["Number"]= $n++; 
}
echo "<pre>";
echo "<p><h3>Output</h3></P>";
print_r($newArr);//Print new array
?>