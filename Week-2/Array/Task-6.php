<!-- PROBLEM : Find the sum of all elements of the below arrays.

$data1 = [0, 1, 2, 3, 4, 5, 6];

$data2 = ['0', '1', '2', '3', '4', '5', '6'];

$data3 = [0, 1, 2, 3, 4, 5, 6, '0', '1', '2', '3', '4', '5', '6'];

$data3 = [0, 1, 2, 3, 4, 5, 6, '0', '1', '2', '3', '4', '5', '6', 'a', 'b', 'c']; -->

<?php

 echo "<b><h1>Task-6</h1></b>"; 

$data1 = [0, 1, 2, 3, 4, 5, 6];//Input
echo "Sum of data1 is : " ;
echo array_sum($data1);//Output Using "array_sum" fun.
echo "<br>";


 $data2 = ['0', '1', '2', '3', '4', '5', '6'];
 $intdata2 = array_map(
     function($value)
     {return (int)$value;} ,$data2
 );
 echo "Sum of data2 is : ";
 echo array_sum($intdata2);
 echo "<br>";

 $data3 = [0, 1, 2, 3, 4, 5, 6, '0', '1', '2', '3', '4', '5', '6'];
 $intdata3 = array_map(
    function($value)
    {return (int)$value;} ,$data3
);
 echo "Sum of data3 is : ";
 echo array_sum($intdata3);
 echo "<br>";

 
 $data3 = [0, 1, 2, 3, 4, 5, 6, '0', '1', '2', '3', '4', '5', '6', 'a', 'b', 'c'];
 $intdata3 = array_map(
    function($value)
    {return (int)$value;} ,$data3
);
// echo "<pre>";
// var_dump($intdata3);
 echo "Sum of data3 is : ";
 echo array_sum($intdata3);

?>