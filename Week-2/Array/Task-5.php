<!-- PROBLEM : You have an array of transactions, each of which has a debit and credit amount. Find the absolute value of the transaction amount (i.e. `abs( debit - credit )`) and add it as a new `key=>value` pair to each transaction.

// For Example

// INPUT
$transactions = [
  [
    "debit" => 2,
    "credit" => 3
  ],
  [
    "debit" => 15,
    "credit" => 14
  ]
];

// OUTPUT
$transactions = [
  [
    "debit" => 2,
    "credit" => 3,
    "amount" => 1
  ],
  [
    "debit" => 15,
    "credit" => 14,
    "amount" => 1
  ]
]; -->

<?php
    echo "<b><h1>Task-5</h1></b>"; 

    $transactions = [
    [
        "debit" => 2,
        "credit" => 3
    ],
    [
        "debit" => 15,
        "credit" => 14
    ]
    ];

    echo "<pre>";
    echo "<p><h3>Input</h3></P>";
    print_r($transactions);

    $new_transactions = array();
foreach( $transactions as $transaction ) {
    $new_transaction = $transaction;
    $new_transaction[ "amount" ] = abs( $transaction[ "debit" ] - $transaction[ "credit" ] );
    $new_transactions[] = $new_transaction;
}
// $transactions = $new_transactions;
    echo "<pre>";
    echo "<p><h3>Output</h3></P>";
    print_r($new_transactions);
?>



