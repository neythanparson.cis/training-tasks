<?php
// Declaring both the numbers
echo "<b><p>(1).Using XOR(^=)</p></b>";
$a = 9;
$b = 4;
echo "Value of x is" . $a . "<br>";
echo "Value of y is" . $b . "<br><br>"; 
// Swapping numbers
$a ^= $b ^= $a ^= $b;
echo "<b>->After swapping:</b><br>";  
    echo "Value of New x =".$a . "<br>" ."Value of New y=".$b ;  
        echo "<br>";
        echo "<br>";
        echo "<br>";
?>


<!-- Using "+" and "-" -->
<?php  
echo "<b><p>(2).Using + and -</p></b>";
$a=9;  
$b=4;  
echo "Value of x is" . $a . "<br>";
echo "Value of y is" . $b . "<br><br>"; 
//using arithmetic operation  
$a=$a+$b;  
$b=$a-$b;  
$a=$a-$b;  
echo "<b>->After swapping:</b><br>";  
    echo "Value of New x =".$a . "<br>" ."Value of New y=".$b ;  
        echo "<br>";
        echo "<br>";
        echo "<br>";
?>  


<!-- Using "*" and "/" -->
<?php  
echo "<b><p>(3).Using * and /</p></b>";
$a=9;  
$b=4;  
echo "Value of x is" . $a . "<br>";
echo "Value of y is" . $b . "<br><br>"; 
//using arithmetic operation  
$a=$a*$b;  
$b=$a/$b;  
$a=$a/$b;  
echo "<b>->After swapping:</b><br>";  
    echo "Value of New x =".$a . "<br>" ."Value of New y=".$b ;  
?>  
