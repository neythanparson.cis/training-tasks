<!-- Get Value from User and find Square root -->

<form method="post">
<label>Enter a number</label>
<input type="text" name="input" value="" />
<input type="submit" name="submit" value="Submit" />
</form>
<?php
    if($_POST)
    {
        //storing the number in a variable $input
        $input = $_POST['input'];
        //storing the square root of the number in a variable $ans
        $ans = sqrt($input); //sqrt is a function for square root. 
        //printing the result
        echo 'The square root of '.$input.' is '.$ans . '.';
    }
?>



<!-- Simple method to find square root -->
<!-- <?php
// simple example to find how sqrt() function works on numbers
echo "Square root of 16 is: " . sqrt(16);
echo '<br>';
?> -->

<!-- without usimg  fun. -->
<!-- <?php
// Using Pow
echo '<br>'.'Square root of 16 is: '. pow(16, 1/2);
?> -->

