<!-- LCM stands for Least Common Multiple. The LCM of two numbers is the smallest number that can be divided by both numbers.
For example - LCM of 20 and 25 is 100 and LCM of 30 and 40 is 120. -->

<form method="post">
<table>
<tr>
    <td><label>Enter a First number: </label></td>
    <td> <input type="text" name="num1" value="" /> </td><br>
</tr>
<tr>
    <td><label>Enter a Second number: </label></td>
    <td> <input type="text" name="num2" value="" /> </td><br>
</tr>
<tr>
    <td> <input type="submit" name="submit" value="Submit"/> </td>
</tr>
</table>
</form>
<?php
if($_POST)
{
    $x = $_POST['num1'];
    $y = $_POST['num2'];

    function lcm($x, $y) { 
        $large = max($x,$y); 
        $small = min($x,$y); 
        $i = $large;
      
        while (true) {
          if($i % $small == 0)
            return $i;
          $i = $i + $large;    
        }  
      }  
      echo "LCM of $x and $y is: ".lcm($x,$y);
}


// // LCM Using GCD
// if($_POST)
//     {
//         $x = $_POST['num1'];
//         $y = $_POST['num2'];
//     if ($x > $y) 
//     {
//         $temp = $x;
//         $x = $y;
//         $y = $temp;
//     }
//     for($i = 1; $i < ($x+1); $i++) 
//     {
//         if ($x%$i == 0 and $y%$i == 0)
//             $gcd = $i;
//     }
//         $lcm = ($x*$y)/$gcd;
//         echo "LCM of $x and $y is: $lcm";
// }