<!-- require will produce a fatal error (E_COMPILE_ERROR) and stop the script
include will only produce a warning (E_WARNING) and the script will continue -->

<!-- there is one big difference between include and require; when a file is included with the include statement and PHP cannot find it, the script will continue to execute: -->

<!DOCTYPE html>
<html>
<body>

<h1>Welcome to my home page!</h1>
<p>Hiii.</p>
<p>Henlo.</p>
<?php include 'copyright.php';?>
<!-- we have a standard footer file called "Copyright.php" -->

</body>
</html>


<!-- If we do the same example using the require statement, the echo statement will not be executed because the script execution dies after the require statement returned a fatal error -->


<!DOCTYPE html>
<html>
<body>

<h1>Welcome to my home page!</h1>
<p>Hiii.</p>
<p>Henlo.</p>
<?php require 'noFileExist.php';?>
<!-- we dont have any php file to include -->
<!-- so it will throw an error  -->
</body>
</html>

