<!-- The fgetc() function is used to read a single character from a file -->

<!DOCTYPE html>
<html>
<body>

<?php
$myfile = fopen("Numbers.txt", "r") or die("Unable to open file!");
// Output one character until end-of-file
while(!feof($myfile)) {
  echo fgetc($myfile);
}
fclose($myfile);
?>

</body>
</html>