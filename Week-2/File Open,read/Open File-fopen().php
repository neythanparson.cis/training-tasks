<!DOCTYPE html>
<html>
<body>

<?php
$myfile = fopen("Numbers.txt", "r") or die("Unable to open file!");
// The first parameter of fopen() contains the name of the file to be opened and the second parameter specifies in which mode the file should be opened. 
echo fread($myfile,filesize("Numbers.txt")) ;
// The first parameter of fread() contains the name of the file to read from and the second parameter specifies the maximum number of bytes to read.
fclose($myfile) ;
// The fclose() requires the name of the file (or a variable that holds the filename) we want to close
?>

</body>
</html>