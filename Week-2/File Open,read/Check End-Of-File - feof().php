<!-- The feof() function checks if the "end-of-file" (EOF) has been reached. -->
<!-- The feof() function is useful for looping through data of unknown length. -->


<!DOCTYPE html>
<html>
<body>

<?php
$myfile = fopen("Numbers.txt", "r") or die("Unable to open file!");
// Output one line until end-of-file
while(!feof($myfile)) {
  echo fgets($myfile) . "<br>";
}
fclose($myfile);
?>

</body>
</html>

