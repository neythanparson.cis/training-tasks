<!-- strpos() function searches for a specific text within a string. If a match is found, the function returns the character position of the first match. If no match is found, it will return FALSE. -->


<!DOCTYPE html>
<html>
<body>

<?php
echo strpos("Hey There,i'am Nikhil!", "Nikhil");
// "nikhil"word starts on 15th position so output will be 15
// The first character position in a string is 0 (not 1).
?> 
 
</body>
</html>
