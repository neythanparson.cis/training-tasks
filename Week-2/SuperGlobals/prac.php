<!-- <?php
// PHP Program to find sum of 
// elements in a given array 

// function to return sum 
// of elements in an array
// of size n
function sum( $arr, $n)
{
    // initialize sum
    $sum = 0; 

    // Iterate through all elements 
    // and add them to sum
    for ($i = 0; $i < $n; $i++)
    $sum += $arr[$i];

    return $sum;
}

// Driver Code
$arr =array("mo1"=> 10,"m02" => 20, "mo3" => 30);
$n = sizeof($arr);
echo "Sum of given array is ", 
                sum($arr, $n);

// This code is contributed by aj_36
?> -->


<?php  
//Initialize array   
$arr = array("m1"=>1,"m2"=>2,"m3"=>3);   
$sum = 0;  
   
//Loop through the array to calculate sum of elements  
for ($i = 0; $i < count($arr); $i++) {   
   $sum = $sum + $arr[$i];  
}    
print("Sum of all the elements of an array: " . $sum);  
?>