<!-- To loop through and print all the values of an indexed array, you could use a for loop -->


<!DOCTYPE html>
<html>
<body>

<?php
$cars = array("Volvo", "BMW", "Toyota");
$arrlength = count($cars);

for($x = 0; $x < $arrlength; $x++)
// $x=0 will start from count 0
// $x++ increment the count by 1
{
  echo $cars[$x];
  echo "<br>";
}
?>

</body>
</html>
