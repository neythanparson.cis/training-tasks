<!-- count() function is used to return the length (the number of elements) of an array -->


<!DOCTYPE html>
<html>
<body>

<?php
$cars = array("Volvo", "BMW", "Toyota" ,"Hyundai");
echo count($cars);
// here we have 4 elementsin the array so output will be 4
?>

</body>
</html>