<!-- To loop through and print all the values of an associative array, you could use a foreach loop -->


<!DOCTYPE html>
<html>
<body>

<?php
$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

foreach($age as $x => $x_value) {
  echo "Key=" . $x . ", Value=" . $x_value;
//   here "peter" , "ben" , "joe" is key
//  here "35" , "37" "43" is value
  echo "<br>";
}
?>

</body>
</html>
