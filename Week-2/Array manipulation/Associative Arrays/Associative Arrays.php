<!-- Associative arrays are arrays that use named keys that you assign to them -->


<!DOCTYPE html>
<html>
<body>

<?php
$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
// here age of all three elements is assigned im array
echo "Peter is " . $age['Peter'] . " years old.";
echo "<br>";
// will check the array end get the value whivh is assigned to "peter" which is 35
echo "Ben is " . $age['Ben'] . " years old.";
?>

</body>
</html>
