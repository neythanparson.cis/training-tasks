<!-- do...while loop - Loops through a block of code once, and then repeats the loop as long as the specified condition is true. -->

<!-- Syntax -do {
  code to be executed;
} while (condition is true); -->

<!DOCTYPE html>
<html>
<body>

<?php  
echo "<p><b>While Loop<b><p>";
$x = 1;
// $x = 1; - Initialize the loop counter ($x), and set the start value to 1
 
do{
  echo "The number is: $x <br>";
  $x++; 
//   $x++; - Increase the loop counter value by 1 for each iteration
}  while ($x <= 15); 
// $x <= 15 - Continue the loop as long as $x is less than or equal to 15
?>

<?php 
echo "<p><b>While Loop to increase loop Counter value by number<b><p>";
$x = 6;
// $x = 6; - Initialize the loop counter ($x), and set the start value to 6
do {
  echo "The number is: $x <br>";
  $x++;
  //   $x++; - Increase the loop counter value by 1 for each iteration
} while ($x <= 50);
// $x <= 50 - Continue the loop as long as $x is less than or equal to 50
?>

