<!-- for loop - Loops through a block of code a specified number of times -->

<!-- Syntax -for (init counter; test counter; increment counter) {
  code to be executed for each iteration;
} -->

<!DOCTYPE html>
<html>
<body>

<?php  
echo "<p><b>For Loop<b><p>";
for ($x = 0; $x <= 10; $x++)
// $x = 0; - Initialize the loop counter ($x), and set the start value to 0
// $x <= 10; - Continue the loop as long as $x is less than or equal to 10
// $x++ - Increase the loop counter value by 1 for each iteration
{
  echo "The number is: $x <br>";
}
?>  

<?php  
echo "<p><b>For Loop<b><p>";
for ($x = 0; $x <= 100; $x+=10) 
// $x = 0; - Initialize the loop counter ($x), and set the start value to 0
// $x <= 100; - Continue the loop as long as $x is less than or equal to 10
// $x+=10 - Increase the loop counter value by 10 for each iteration
{
  echo "The number is: $x <br>";
}
?> 

</body>
</html>

