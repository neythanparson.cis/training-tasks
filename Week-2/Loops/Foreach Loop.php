<!-- foreach loop - Loops through a block of code for each element in an array. -->

<!-- Syntax - foreach ($array as $value) {
  code to be executed;
} -->

<!DOCTYPE html>
<html>
<body>

<?php  
echo "<p><b>Foreach Loop<b><p>";
$colors = array("red", "green", "blue", "yellow"); 

foreach ($colors as $value) {
  echo "$value <br>";
}
?>  

<?php
echo "<p><b>both the keys and the values of the given array (age)<b><p>";

$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

foreach($age as $x => $val) {
  echo "$x = $val<br>";
}
?>

</body>
</html>
