<!-- while - loops through a block of code as long as the specified condition is true -->

<!-- Syntax - while (condition is true) {
  code to be executed;
} -->

<!DOCTYPE html>
<html>
<body>

<?php  
echo "<p><b>While Loop<b><p>";
$x = 1;
// $x = 1; - Initialize the loop counter ($x), and set the start value to 1
 
while($x <= 15)
// $x <= 15 - Continue the loop as long as $x is less than or equal to 15

{
  echo "The number is: $x <br>";
  $x++;
//   $x++; - Increase the loop counter value by 1 for each iteration
} 
?> 

<?php
echo "<p><b>While Loop to increase loop Counter value by number<b><p>";
$x = 0;
// $x = 0; - Initialize the loop counter ($x), and set the start value to 0
 
while($x <= 50)
// $x <= 50 - Continue the loop as long as $x is less than or equal to 50

{
  echo "The number is: $x <br>";
  $x+=5;
//   $x+=5 - Increase the loop counter value by 5 for each iteration
} 

?>  

</body>
</html>