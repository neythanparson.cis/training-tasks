$(document).ready(function() {
    // focus() event
    $('#sname,#sclass,#scountry').focus(function() {
        $(this).css('background-color', 'lime');
    });

    // blur() event
    $('#sname,#sclass,#scountry').blur(function() {
        $(this).css('background-color', '');
    });

    // change()
    $('#scountry').change(function() {
        // $(this).css('background-color', 'pink');
        var a = $(this).val();
        $('#test').html(a);
    });

    // Selection() event
    $('#sname,#sclass').select(function() {
        $(this).css('background-color', 'yellow');
    });



});