$(document).ready(function() {
    // focus() event
    $('.form-control').focus(function() {
        $(this).css('background-color', 'lime');
    });

    // blur() event
    $('.form-control').blur(function() {
        $(this).css('background-color', '');
    });

    // change()
    $('#city').change(function() {
        $(this).css('background-color', 'skyblue');
    });

});
//initially it will be null
var selectedRow = null

function onFormSubmit() {
    var formData = readFormData();
    // if we have value null we'll execute insert operation
    // else we'll execute update operation.
    if (selectedRow == null)
        insertNewRecord(formData);
    else
        updateRecord(formData);
    resetForm();
}

function readFormData() {
    //will PUSH the values
    var formData = {};
    formData["fullName"] = document.getElementById("fullName").value;
    formData["empid"] = document.getElementById("empid").value;
    formData["email"] = document.getElementById("email").value;
    formData["salary"] = document.getElementById("salary").value;
    formData["jdate"] = document.getElementById("jdate").value;
    formData["city"] = document.getElementById("city").value;
    return formData;
}
// inseret form data into table (dynamically)
// inserting cells first and data into it
function insertNewRecord(data) {
    var table = document.getElementById("employeeList").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.fullName;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.empid;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.email;
    cell4 = newRow.insertCell(3);
    cell4.innerHTML = data.salary;
    cell5 = newRow.insertCell(4);
    cell5.innerHTML = data.jdate;
    cell6 = newRow.insertCell(5);
    cell6.innerHTML = data.city;
    cell7 = newRow.insertCell(6);
    cell7.innerHTML = `<button type="button" class="btn btn-success" onClick="onEdit(this)">Edit</button>
                        <button type="button" class="btn btn-danger" onClick="onDelete(this)">Delete</button>`;
}
// will reset the form after submitting it
function resetForm() {
    document.getElementById("fullName").value = "";
    document.getElementById("empid").value = "";
    document.getElementById("email").value = "";
    document.getElementById("salary").value = "";
    document.getElementById("jdate").value = "";
    document.getElementById("city").value = "";
    selectedRow = null;
}
// edit function after clicking an edit button
// it will throw the data from table to form
//p.meter td - cell of table 
function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById("fullName").value = selectedRow.cells[0].innerHTML;
    document.getElementById("empid").value = selectedRow.cells[1].innerHTML;
    document.getElementById("email").value = selectedRow.cells[2].innerHTML;
    document.getElementById("salary").value = selectedRow.cells[3].innerHTML;
    document.getElementById("jdate").value = selectedRow.cells[4].innerHTML;
    document.getElementById("city").value = selectedRow.cells[5].innerHTML;
}
// save updated data to table
function updateRecord(formData) {
    selectedRow.cells[0].innerHTML = formData.fullName;
    selectedRow.cells[1].innerHTML = formData.empid;
    selectedRow.cells[2].innerHTML = formData.email;
    selectedRow.cells[3].innerHTML = formData.salary;
    selectedRow.cells[4].innerHTML = formData.jdate;
    selectedRow.cells[5].innerHTML = formData.city;
}

function onDelete(td) {
    //operation will execute only if u confirm!
    if (confirm('Are you sure to delete this record ?')) {
        row = td.parentElement.parentElement;
        //delete row using delete function
        document.getElementById("employeeList").deleteRow(row.rowIndex);
        resetForm();
    }
}