$(document).ready(function() {
    $("#submit").click(function() {
        var fname, color;
        fname = $("#fullName").val();
        color = $("#colorlist").val();


        $(".error").remove();
        //Name Validation
        if (fname.length < 3) {
            $('#fullName').after('<span class="error">*Please Enter Full Name</span>');
            return false;
        }
        if ($.trim($("#fullName").val()) === "") {
            $('#fullName').after('<span class="error">*You can not add blank Space.</span>');
            return false;
        }
        //Color Validation
        if (color == "") {
            $('#colorlist').after('<span class="error">*Please Choose Color</span>');
            return false;
        }

        //add div dinamically
        $('.box-main-div').append(`<div style=" width: 100px;height: 60px;margin: 5px;text-align: center;padding-top: 15px;font-size: '18pt';border: 3px;background-color:${color}">${fname}</div>`);

        //Reset form
        $('#form').each(function() {
            this.reset();
        });

        $(".box-main-div div").click(function() {
            $(this).remove();
        });

    });
});