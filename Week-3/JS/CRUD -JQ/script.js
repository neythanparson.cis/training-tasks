$(document).ready(function() {
    // focus() event
    $('.form-control').focus(function() {
        $(this).css('background-color', 'lime');
    });

    // blur() event
    $('.form-control').blur(function() {
        $(this).css('background-color', '');
    });

    // change() event
    $('#city').change(function() {
        $(this).css('background-color', 'skyblue');
    });


    var i = 0;
    // add data from form to table
    $("#submit").click(function() {
        var fname, empid, Email, salary, jdate, city;
        fname = $("#fullName").val();
        empid = $("#empid").val();
        Email = $("#email").val();
        salary = $("#salary").val();
        jdate = $("#jdate").val();
        city = $("#city").val();


        $(".error").remove();

        //Fullname Validation
        if (fname.length < 3) {
            $('#fullName').after('<span class="error">*Please Enter Full Name</span>');
            return false;
        }
        if ($.trim($("#fullName").val()) === "") {
            $('#fullName').after('<span class="error">*You can not add blank Space.</span>');
            return false;
        }

        //email validation
        if (Email.length < 1) {
            $('#email').after('<span class="error">*Please Enter E-mail</span>');
            return false;
        } else {
            var regEx = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            var validEmail = regEx.test(Email);
            if (!validEmail) {
                $('#email').after('<span class="error">*Please Enter Valid E-mail</span>');
                return false;
            }
        }

        //Salary Validation
        if (salary.length <= 0) {
            $('#salary').after('<span class="error">*Please Enter Salary</span>');
            return false;
        }

        //Joining Date Validation
        if (jdate == "") {
            $('#jdate').after('<span class="error">*Please Enter Joining date</span>');
            return false;
        }

        //City Validation
        if (city == "") {
            $('#city').after('<span class="error">*Please Choose City</span>');
            return false;
        }


        if (Number(empid)) {
            $('#tr-' + empid).empty().append(`<td>${fname}</td><td>${i}</td><td>${Email}</td><td>${salary}</td><td>${jdate}</td><td>${city}</td><td> <button type='button' onclick="editfun(this,${empid})" class='btn-success btn'>Edit</button> <button type='button' class='btn-danger btn'>Delete</button> </td>`);
        } else {
            // $('.pad').each(function() {
            //     (i.padStart(2, '0'));
            // })

            i++;

            // (str1.padStart(2, '0'));
            var markup = `<tr id="tr-${i}"><td>${fname}</td><td>${i}</td><td>${Email}</td><td>${salary}</td><td>${jdate}</td><td>${city}</td><td> <button type='button' onclick="editfun(this,${i})" class='btn-success btn'>Edit</button> <button type='button' class='btn-danger btn'>Delete</button> </td></tr>`;
            $("#employeeList > tbody").append(markup);
        }

    });

    //delete Row
    $(document).on("click", ".btn-danger", function(e) {
        if (confirm("Are you sure want to delete this record!")) {
            $(this).closest('tr').remove();
        } else {
            e.preventDefault();
        }
    });

    //reset the form after click of submit button
    $('#submit').click(function() {
        $(':input')
            .not(':button') // NOT THE BUTTONS.
            .val('');
    });

});


// edit table
function editfun(obj, i) {
    var row = $(obj).closest('tr');
    $('#employeeList').val($(row).index());
    var td = $(row).find("td");
    $('#fullName').val($(td).eq(0).html());
    $('#empid').val(i);
    $('#email').val($(td).eq(2).html());
    $('#salary').val($(td).eq(3).html());
    $('#jdate').val($(td).eq(4).html());
    $('#city').val($(td).eq(5).html());
}