 
 <!-- Main Footer -->
  <footer class="main-footer">
      <strong>Copyright &copy;  2021- {{date("Y")}} <a href="https://www.instagram.com/_photugrapher_/">Nikhil Patel</a>.</strong> All rights reserved.
      <div class="float-right d-none d-sm-inline-block">
      Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})
      </div>
  </footer>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->
  <!-- jQuery -->
  <script src="{{asset('assets/theme/plugins/jquery/jquery.min.js')}}"></script>
  <!-- Bootstrap -->
  <script src="{{asset('assets/theme/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- AdminLTE App -->
  <script src="{{asset('assets/theme/js/adminlte.js')}}"></script>

  <!-- PAGE PLUGINS -->
  <!-- jQuery Mapael -->
  <script src="{{asset('assets/theme/plugins/jquery-mousewheel/jquery.mousewheel.js')}}"></script>
  <script src="{{asset('assets/theme/plugins/raphael/raphael.min.js')}}"></script>
  <script src="{{asset('assets/theme/plugins/jquery-mapael/jquery.mapael.min.js')}}"></script>
  <script src="{{asset('assets/theme/plugins/jquery-mapael/maps/usa_states.min.js')}}"></script>
  <script src="{{asset('assets/theme/plugins/select2/js/select2.full.min.js')}}"></script>
  <script src="{{asset('assets/theme/plugins/sweetalert2/sweetalert2.all.min.js')}}"></script>
  <script src="{{asset('assets/theme/plugins/toastr/toastr.min.js')}}"></script>

  <script src="{{asset('assets/custom.js')}}"></script>

  
  <script src="{{asset('assets/theme/js/pages/dashboard2.js')}}"></script>

  <script>
    $(document).ready(function(){
        $(".select2").select2({
          placeholder: "Please Select",
        });
        
    });
</script>
<script>
   $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-dismissible").alert('close');
});
</script>
  @stack('script')
  </body>

  </html>