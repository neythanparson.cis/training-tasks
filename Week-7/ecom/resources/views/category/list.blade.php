@extends('layout.app')
@section('title', 'Category')
@section('head', 'Categories')
@section('main')
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <a href="{{ route('category.categoryAdd') }}" type="button" class=" btn btn-primary"><i
                        class="fa fa-plus" aria-hidden="true">&nbsp;&nbsp;</i> Add New Record!</a>

                <div class="float-right form-inline ">
                    <input type="text" name="categoy-search" placeholder="Category Name" id="categoy-search"
                        class="form-control mb-3">

                    <select class="form-control  ml-2 mb-3" name="status" id="status">
                        <option value="" selected>Status</option>
                        <option value="'1'">Active</option>
                        <option value="'0'">Deactive</option>
                    </select>

                    <input type="submit" value="Search" name="search" class="btn btn-info ml-2 mb-3"></input>
                    <input type="submit" value="Reset" name="reset" id="reset" class="btn btn-danger ml-2 mb-3"></input>
                </div>
            </div>
            <div class="card-body p-0">
                <table class="table table-striped projects">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th style="width: 15%">Added Date</th>
                            <th style="width: 15%">Modified Date/Time</th>
                            {{-- <th>No. of Products</th> --}}
                            <th>Order</th>
                            <th>Status</th>
                            <th style="width: 10%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!empty($categoryList))
                        {{-- {!! gettype($categoryList) !!} --}}
                            @foreach ($categoryList as $show)
                                <tr>
                                    <td>{{ $show->id }}</td>
                                    <td>
                                        <img alt="Avatar" class="table-avatar tbl-img"
                                            src="{{ asset('image/category/' . $show->v_image) }}">
                                    </td>
                                    <td>{{ $show->v_name }}</td>
                                    <td>{{ $show->dt_added_on }}</td>
                                    <td>{{ $show->dt_modified_on }}</td>
                                    {{-- <td></td> --}}
                                    <td>{{ $show->i_order }}</td>
                                    <td>
                                        @if ($show->ti_status == 1)
                                            <a class='btn btn-success btn-sm'
                                                href='?type=ti_status&operation=deactive&id=""'><i
                                                    class="fas fa-toggle-on"></i></a>
                                        @else
                                            <a class='btn btn-warning btn-sm'
                                                href='?type=ti_status&operation=active&id=""'><i
                                                    class="fas fa-toggle-off"></i></a>
                                        @endif
                                    </td>
                                    <td class="project-actions text-centre">
                                        <a class="btn btn-info btn-sm"
                                            href="{{ route('category.categoryEdit', $show->id) }}"><i
                                                class="fas fa-pencil-alt"></i></a>
                                        <button class="btn btn-danger btn-sm show_confirm" data-id="{{ $show->id }}"
                                            href=""><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        {{-- @else
                        <tr>
                            <td colspan="8"><h4 style="text-align: center">NO RESULTS FOUND</h4></td>
                        </tr> --}}
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection
@push('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.show_confirm').click(function(e) {
                e.preventDefault;
                var el = $(this)
                // console.log(el);
                var delete_id = $(this).data('id');
                // alert(delete_id);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert Data!",
                    // icon: 'warning',
                    imageUrl: "assets/theme/img/error.gif",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var data = {
                            "_token": "{{ csrf_token() }}",
                            "id": delete_id,
                        };
                        jQuery.ajax({
                            url: "{!! route('category.categoryDelete') !!}",
                            type: "POST",
                            data: data,
                            dataType: "json",
                            success: function(res) {
                                $(el).closest('tr').fadeOut(800, function() {
                                    $(this).remove();
                                });

                                if (res) {
                                    Swal.fire(
                                        'Deleted!',
                                        'Your file has been deleted.',
                                        'success'
                                    )
                                }

                            }
                        });
                    }
                })
            });
        });
    </script>
@endpush
