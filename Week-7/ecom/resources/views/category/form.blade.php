@extends('layout.app')

@if ($category->id == '')
    @section('title', 'Add-Category')
    @section('head', 'Add-Category')
@else
    @section('title', 'Edit-Category')
    @section('head', 'Edit-Category')
@endif
@push('style')
    <style>
        .hide {
            display: none;
        }

        .show {
            display: block;
        }

        .image-preview__image {
            width: 120px;
            height: 150px;
        }

    </style>
@endpush

@section('main')

    <section class="content">
        <div class="card card-primary">
            <form action="{!! $urlSave !!}" method="POST" enctype="multipart/form-data" id="saveForm">
                @csrf
                <div class="card-body">

                    <div class="image-preview {{ isset($category->v_image) ? 'show' : 'hide' }} ">
                        <img id="fileList" class="image-preview__image"
                            src="{{ $category->v_image ? asset('image/category/' . $category->v_image) : '' }}">
                    </div>

                    <div class="form-group">
                        <label for="categories">Category Image: </label>
                        <input name="v_image" accept="image/*" type="file" id="fileElem" value="{!! $category->v_image !!}"
                            class="form-control" onchange="preview(event)">
                        <span class="text-danger error-text error_err"></span>
                    </div>
                    <div class="form-group">
                        <label for="categories">Category Name: </label>
                        <input name="v_name" type="text" placeholder="Enter Category" class="form-control"
                            value="{!! $category->v_name !!}">
                    </div>
                    <div class="form-group">
                        <label for="categories">Orders:</label>
                        <input name="i_order" type="text" placeholder="Enter Order" class="form-control"
                            value="{!! $category->i_order !!}">
                    </div>
                    <div class="form-group">
                        <label for="categories">Status:</label>
                        <select name="ti_status" class="form-control custom-select" id="myselect">
                            <option value="" selected>Status</option>
                            <option value="1">Active</option>
                            <option value="0">Deactive</option>
                        </select>
                    </div>
                    <button name="submit" type="submit" class="btn btn-lg btn-info btn-block">
                        <span name="submit">Submit</span>
                    </button>
                </div>
            </form>
        </div>
    </section>
@endsection
@push('script')
    <script type='text/javascript'>
        var preview = function(event) {
            var output = document.getElementById('fileList');
            output.src = URL.createObjectURL(event.target.files[0]);
            $('.image-preview').addClass('show');
            $('.image-preview').removeClass('hide');
        };

        jQuery("#saveForm").submit(function(e) {
            e.preventDefault();
            var data = new FormData($('#saveForm')[0]);
            var url = "{!! $urlSave !!}";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                mimeType: "multipart/form-data",
                dataType: "json",
                contentType: false,
                processData: false,
                // timeout: 5000,
                success: function(response) {
                     console.log(response);
                    if ($.isEmptyObject(response.err)) {
                        printSuccessMsg(response.success);

                        // setTimeout(() => {
                            window.location.href = response.url;
                        // }, 500);

                    } else {
                        printErrorMsg(response.err);
                    }
                }

            });

        });
    </script>
@endpush
