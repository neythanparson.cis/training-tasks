@extends('layout.app')
@section('title','Add-Product')
@section('head','Add-Product')

@section('main')
<section class="content">
    <div class="card card-primary">
        <div class="card-body">
            <div class="form-group">
                <label data-placeholder="Choose Categories" for="product">Categories:</label>
                <select multiple class="select2 form-control" name="test">
                    <option value=""></option>
                    <option>American Black Bear</option>
                    <option>Asiatic Black Bear</option>
                    <option>Brown Bear</option>
                    <option>Giant Panda</option>
                    <option>Sloth Bear</option>
                    <option>Sun Bear</option>
                    <option>Polar Bear</option>
                    <option>Spectacled Bear</option>
                </select>
            </div>
            <div class="form-group">
                <label for="product">Product Image: </label>
                <input type="file" class="form-control" id="upload_file" onchange="preview_image(event)">
            </div>
            <div class="form-group">
                <label for="product">Product Name: </label>
                <input type="text" placeholder="Enter Product" class="form-control">
            </div>
            <div class="form-group">
                <label for="product">Price:</label>
                <input type="text" placeholder="Enter Order" class="form-control">
            </div>
            <div class="form-group">
                <label for="product">Sale Price:</label>
                <input type="text" placeholder="Enter Order" class="form-control">
            </div>
            <div class="form-group">
                <label for="product">Quantity:</label>
                <input type="text" placeholder="Enter Order" class="form-control">
            </div>
            <div class="form-group">
                <label for="product">Orders:</label>
                <input type="text" placeholder="Enter Order" class="form-control">
            </div>
            <div class="form-group">
                <label for="product">Status:</label>
                <select class="form-control custom-select">
                    <option value="" selected>Status</option>
                    <option value="1">Active</option>
                    <option value="0">Deactive</option>
                </select>
            </div>
            <button name="submit" type="submit" class="btn btn-lg btn-info btn-block">
                <span name="submit">Submit</span>
            </button>
        </div>
    </div>
</section>
@endsection