@extends('layout.app')
@section('title','Product')
@section('head','Products')

@section('main')
<section class="content">
    <div class="container-fluid">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <a href="productform" type="button" class=" btn btn-primary ml-3 "><i class="fa fa-plus" aria-hidden="true">&nbsp;&nbsp;</i> Add New Record!</a>

                    <input type="text" name="product-search" placeholder="Product Name" class="form-control col-2 ml-5">

                    <label for="" class="ml-2 mt-2">Price-Range:</label>
                    <input type="text" name="min-price" placeholder="Min-price" class="form-control col-1 ml-1">
                    <input type="text" name="max-price" placeholder="Max-price" class="form-control col-1">

                    <label for="" class="ml-2 mt-2">Qty-Range:</label>
                    <input type="text" name="min-qty" placeholder="Min-qty" class="form-control col-1 ml-1">
                    <input type="text" name="max-qty" placeholder="Max-qty" class="form-control col-1">

                    <select class="form-control col-1 ml-2" id="status" name="status">
                        <option value="" selected>Status</option>
                        <option value="'1'">Active</option>
                        <option value="'0'">Deactive</option>
                    </select>

                    <a class="btn btn-info btn-md ml-2" href="#"><i class="fas fa-search"></i></a>
                    <a class="btn btn-danger btn-md ml-2" href="#"><i class="fas fa-undo"></i></a>
                </div>
            </div>
        </div>
        <div class="card-body p-0">
            <table class="table table-striped projects">
                <thead>
                    <tr>
                        <th>ID </th>
                        <th>Image</th>
                        <th style="width: 10%">Name</th>
                        <th>Product Code</th>
                        <th>Categorie Name</th>
                        <th>Price</th>
                        <th>Sale_price</th>
                        <th>Qty</th>
                        <th style="width: 20%">Added Date</th>
                        <th style="width: 20%">Modified Date/Time</th>
                        <th>NO. of Products</th>
                        <th>Order</th>
                        <th>Status</th>
                        <th style="width: 9%">Action</th>
                    </tr>
                </thead>
                <tbody>
              
                    <tr>
                        <td>#</td>
                        <td>
                            <img alt="Avatar" class="table-avatar tbl-img" src="{{asset('assets/theme/img/avatar.png')}}">
                        </td>
                        <td>
                            AdminLTE v3
                        </td>
                        <td>69</td>
                        <td>Rings</td>
                        <td>12</td>
                        <td>100</td>
                        <td>5</td>
                        <td>20-04-2022/14:11:39</td>
                        <td>20-04-2022/14:11:39</td>
                        <td>69</td>
                        <td>23</td>
                        <td>
                            <a class='btn btn-success btn-sm' href='?type=ti_status&operation=deactive&id=""'><i class="fas fa-toggle-on"></i></a>
                            <a class='btn btn-warning btn-sm' href='?type=ti_status&operation=active&id=""'><i class="fas fa-toggle-off"></i></a>
                        </td>
                        <td class="project-actions text-centre">
                            <a class="btn btn-info btn-sm" href="#"><i class="fas fa-pencil-alt"></i></a>
                            <a class="btn btn-danger btn-sm" href="#"><i class="fas fa-trash"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection