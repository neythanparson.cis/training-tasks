<?php

use App\Http\Controllers\adminLogin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Form;
use App\Http\Controllers\category;
use GuzzleHttp\Middleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// route::view('index','index')->name('index');




route::get('/', 'login');

Route::group(['middleware' => 'guest'], function () {
    Route::get('login', [adminLogin::class, 'index'])->name('login.index');
    Route::post('login/submit', [adminLogin::class, 'login'])->name('login.submit');
});


Route::group(['middleware' => 'adminAuth'], function () {
    Route::get('/logout', [adminLogin::class, 'logout'])->name('logout.perform');
    Route::get('index', [adminLogin::class, 'display'])->name('index');

    Route::get('category', [category::class, 'index'])->name('category.index');//category table 
    Route::get('categoryadd', [category::class, 'form'])->name('category.categoryAdd');//category add (form)
    Route::get('category/edit/{id}', [category::class, 'form'])->name('category.categoryEdit');//category edit(form)
    Route::post('categorysave/{id}', [category::class, 'save'])->name('category.categorySave');//category save (add/update)
    Route::post('category/delete', [category::class, 'delete'])->name('category.categoryDelete');
});

// route::view('category','category.list');
// route::view('categoryform','category.form');

// route::view('product','product.list');
// route::view('productform','product.Form');



// Route::get('web', function () {
//     return view('web',['data'=>['name'=>'Nikhil','age'=>'23']]);
// });
