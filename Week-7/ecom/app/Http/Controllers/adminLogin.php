<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Auth;
use App\Models\tbl_admin_users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class adminLogin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'Required',
            'password' => 'Required'
        ]);
        
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect('index')->with('success','welcome' ." ". username());
        } else {
            return redirect('login')->with('error','Please enter valid login details!');
        }
    }
    public function display()
    {
        // $user = Auth::user();

        // dd($user);
        return view('index');
    }

    public function logout()
    {
        Auth::logout(); 
        return redirect('login');
    }
}
