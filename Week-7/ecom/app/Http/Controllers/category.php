<?php

namespace App\Http\Controllers;

use App\Models\tbl_categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;


class category extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categoryList  = tbl_categories::all();
        // dd($categoryList);
        return view('category.list', ['categoryList' => $categoryList]);
    }

    public function form($id = 0)
    {
        $category = tbl_categories::find($id);
        $category = $category ? $category : new tbl_categories;
        $urlsave = $id ? route('category.categorySave', [$category->id]) : route('category.categorySave', [0]);
        // dd($urlsave);
        return view('category.form', ['category' => $category, 'urlSave' => $urlsave]);
    }

    public function save($id, Request $request)
    {
        $msg = [
            "v_name.required" => 'Category Name is Required!',
            "v_name.unique" => 'Category Name Already Exists!',
            "v_image.required" => 'Please Select Image!',
            "i_order.required" => 'Order is Required!',
            "ti_status.required" => 'Please Select Status!'
        ];

        $validator = validator::make($request->all(),
          [
            'v_name' => 'required',
            'i_order' => 'required|numeric',
            'ti_status' => 'required'

        ],$msg);

        if (!$id) {
        $validator = validator::make($request->all(), [
            'v_image' => 'required',
            'v_name' => 'required|unique:tbl_categories,v_name',
            'i_order' => 'required|numeric',
            'ti_status' => 'required'
            
        ],$msg);
    }
    // dd($validator->errors()->all()[0]);
        if($validator->fails()){
            // dd($err);
            return response()->json(['err'=>$validator->errors()->all()[0]]);
        }

        $ext = ['jpg', 'jpeg', 'png', 'gif'];
        $category = tbl_categories::find($id);
        $category = $category ? $category : new tbl_categories;

        if ($request->v_image) {
            if (in_array($request->v_image->extension(), $ext)) {
                $image_name = time() . '_' . $request->categoryname . '.' . $request->v_image->extension();
                $request->v_image->move(public_path('image/category'), $image_name);
                $category->v_image = $image_name;
            } else {
                return response()->json(['err' => "Image must be in jpg/jpeg/png/gif format"]);
            }
        }
        $category->v_name = $request->v_name;
        $category->i_order = $request->i_order;
        $category->ti_status = $request->ti_status;

        if ($category->save()) {

            return response()->json(['success' => 'Category Add Success','url'=>route('category.index')]);
        }
    }

    public function delete( Request $request)
    {
        $category = tbl_categories::find($request->id);
        $category->delete();
        // return response()->json(['status' => 'Deleted']);
        //return redirect('category')->with('success', 'Record has been deleted successfully');
    }
}
