<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_categories extends Model
{
    public $table = 'tbl_categories';
    public $primaryKey = 'id';
    public $timestamps = false;
    public $fillable = ['v_image', 'v_name', 'dt_added_on', 'dt_modified_on', 'i_order', 'ti_status'];
    public $rules = [
        'v_name' => 'required|v_name|unique:tbl_categories',
       
    ];

}
