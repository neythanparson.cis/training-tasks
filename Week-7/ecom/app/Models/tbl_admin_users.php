<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_admin_users extends Model
{
    protected $table = 'tbl_admin_users';
    protected $primaryKey = 'id';
    protected $fillable = ['v_username', 'v_password'];
}
