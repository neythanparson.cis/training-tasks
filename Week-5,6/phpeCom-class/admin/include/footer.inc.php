<div class="clearfix"></div>
<footer class="site-footer">
   <div class="footer-inner bg-white">
      <div class="row">
         <div class="col-sm-6">
            Copyright &copy; 2021-<?php echo date("Y"); ?> Nikhil Patel
         </div>
      </div>
   </div>
</footer>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="assets/js/vendor/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="assets/js/popper.min.js" type="text/javascript"></script>
<script src="assets/js/plugins.js" type="text/javascript"></script>

<script src="assets/js/main.js" type="text/javascript"></script>
<script src="assets/js/custom.js" type="text/javascript"></script>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>



<script>
      //  reset button
      $(document).on('click', '#reset', function() {
        $("#status").val('');
        $("#change").submit();
    });
</script>
<script>
   $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-dismissible").alert('close');
});
</script>


</body>

</html>