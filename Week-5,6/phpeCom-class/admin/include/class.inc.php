<?php
session_start();

class DB_con
{
	private $server='localhost';
	private $username='root';
	private $password='';
	private $dbname='ecommerce';
	public $result;
	private $data;
	public $row;
	public $error;
	public $last_id;


	public $con='';

	function __construct()
	{
		$this->con = new mysqli($this->server,$this->username,$this->password,$this->dbname);

		if ($this->con->connect_error) {
			die("Connection failed: " . $this->con->connect_error);
		  }
	}

	function select($fields, $table,$Join='',$where = '',$search=[] , $group = '')
	{
			// print_r($search);
			// exit;
		if($search != '') {
            $searchArray = [];
            foreach ($search as $key => $val) {
                $searchArray[] = "AND " . "$key  $val";
            }

		$sql_select = "select " . $fields . " from " . $table . $Join . ' where 1 ' . $where . implode($searchArray) . $group ;
			// echo $sql_select;
			// exit;
		$this->result = $this->con->query($sql_select);
		if ($this->result->num_rows > 0) {
			while ($this->row = $this->result->fetch_assoc()) {
				$this->data[] = $this->row;
			}
		}
		return $this->data;
		}
	}

	public function insertData($table, $fields)
	{	
		$sql_insert="INSERT INTO " .$table. "(`".implode("`,`",array_keys($fields))."`) VALUES ('".implode("','",$fields)."');";
		// echo $sql_insert;
		// exit;
		$this->result=$this->con->query($sql_insert);
	   $last_id = $this->con->insert_id;
	   return $last_id;
	  //exit;
	}

	public function updateData($table, $condition_arr, $where_field, $where_value)
	{
		if ($condition_arr != '') {
			$sql = "update $table set ";
			$c = count($condition_arr);
			$i = 1;
			foreach ($condition_arr as $key => $val) {
				$i == $c ? $sql .= "$key='$val'" : $sql .= "$key='$val', ";
				$i++;
			}
			$sql .= " where $where_field='$where_value' ";
		// echo $sql;
		// exit;
			$this->result = $this->con->query($sql);
			// echo $this->result;
			// exit;
		}
		return $this->result;
	}


	public function deleteData($table, $condition_arr)
	{
		if ($condition_arr != '') {
			$sql = "delete from $table where ";
			$c = count($condition_arr);
			$i = 1;
			foreach ($condition_arr as $key => $val) {
				$i == $c ? $sql .= "$key='$val'" : $sql .= "$key='$val' and ";
				$i++;
			}
			$this->result = $this->con->query($sql);
		}
	}
	public function input_data($data)
	{
    	$data = trim($data);
 		$data = stripslashes($data);
    	$data = htmlspecialchars($data);
    	return $data;
	}
}
?>