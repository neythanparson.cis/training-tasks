<?php
$title = "Categories";
require('../include/top_2.inc.php');
$msg = $str = $statusSearch = '';


$catList = new DB_con;

//STATUS (ACTIVE/DEACTIVE)
if (isset($_GET['type']) && $_GET['type'] != '') {
	$type = $_GET['type'];
	if ($type == 'ti_status') {
		$operation = $_GET['operation'];
		$id = $_GET['id'];
		($operation == 'active') ? $status = '1' : $status = '0';
		$condition_arr = ['ti_status' => $status];
		$catList->updateData('tbl_categories', $condition_arr, 'id', $id);
	}
}

//Delete
if (isset($_GET['type']) &&  $_GET['type'] != '') {
	$type = $_GET['type'];
	if ($type == 'delete') {
		$id = $_GET['id'];
		$condition_arr = ['id' => $id];
		$catList->deleteData('tbl_categories', $condition_arr);
	}
}


if (isset($_POST['search'])) {
	$str = $_POST['categoy-search'];
	$statusSearch = $_POST['status'];

	$group = " GROUP BY tbl_categories.id";
	$main = 'tbl_categories.id,tbl_categories.v_categories,tbl_categories.v_image,tbl_categories.i_order,tbl_categories.ti_status,tbl_categories.dt_addon_date,tbl_categories.dt_modified_date,COUNT(tbl_product.v_categories) as total';
	$rightJoin = 'RIGHT JOIN tbl_categories ON tbl_product.v_categories=tbl_categories.id';

	if (!empty($_POST['categoy-search']) && !empty($_POST['status'])) {
		$search = ['tbl_categories.v_categories' => "LIKE '%$str%'", 'tbl_categories.ti_status' => "= $statusSearch"];
		$result = $catList->select($main, ' tbl_product ', $rightJoin, " ", $search, $group);
	} 
	else if (empty($_POST['categoy-search']) && !empty($_POST['status'])) {
		$search = ['tbl_categories.ti_status' => "= $statusSearch"];
		$result = $catList->select($main, ' tbl_product ', $rightJoin, " ", $search , $group);
	} 
	else if (!empty($_POST['categoy-search']) && empty($_POST['status'])) {
		$search = ['tbl_categories.v_categories' => " LIKE '%$str%'"];
		$result = $catList->select($main, ' tbl_product ', $rightJoin, " ", $search, $group);
	} 
	else {
		$result = $catList->select('tbl_categories.id,tbl_categories.v_categories,tbl_categories.v_image,tbl_categories.i_order,tbl_categories.ti_status,tbl_categories.dt_addon_date,tbl_categories.dt_modified_date,COUNT(tbl_product.v_categories) as total', ' tbl_product ', 'RIGHT JOIN tbl_categories ON tbl_product.v_categories=tbl_categories.id', 'GROUP BY tbl_categories.id');
	}
}

if (!isset($_POST['search'])) {
	$result = $catList->select('tbl_categories.id,tbl_categories.v_categories,tbl_categories.v_image,tbl_categories.i_order,tbl_categories.ti_status,tbl_categories.dt_addon_date,tbl_categories.dt_modified_date,COUNT(tbl_product.v_categories) as total', ' tbl_product ', 'RIGHT JOIN tbl_categories ON tbl_product.v_categories=tbl_categories.id', 'GROUP BY tbl_categories.id');
}
?>

<form action="" method="post">
	<div class="content pb-0">
		<div class="orders">
			<div class="row ">
				<div class="col-xl-12 grid-margin stretch-card">
					<form action="" id="change">
						<!-- <form class="form-inline "> -->
						<a href="categoryForm.php" type="button" class=" btn btn-primary mb-3"><i class="fa fa-plus" aria-hidden="true">&nbsp;&nbsp;</i> Add New Record!</a>
						<div class="float-right form-inline ">
							<input type="text" name="categoy-search" placeholder="Category Name" id="categoy-search" class="form-control mb-3" value="<?php echo $str ?>">

							<select class="form-control  ml-2 mb-3" name="status" id="status">
								<option value="" selected>Status</option>
								<option value="'1'" <?php echo (isset($_POST['status']) && $_POST['status'] == "'1'") ? 'selected' : '' ?>>Active</option>
								<option value="'0'" <?php echo (isset($_POST['status']) && $_POST['status'] == "'0'") ? 'selected' : '' ?>>Deactive</option>
							</select>

							<input type="submit" value="Search" name="search" class="btn btn-primary ml-2 mb-3"></input>
							<input type="submit" value="Reset" name="reset" id="reset" class="btn btn-danger ml-2 mb-3"></input>

						</div>
					</form>
					<div class="field_error"><?php echo $msg ?></div>

					<!-- </form> -->

					<div class="card ">

						<div class="card-body">
							<?php
							if (isset($_SESSION['alrt'])) {
							?>
								<div class="alert alert-success alert-dismissible fade show" role="alert">
									<strong>Bro! </strong><?php echo $_SESSION['alrt']; ?>
								</div>
							<?php
								unset($_SESSION['alrt']);
							}
							?>
							<h4 class="box-title mb-3">Categories </h4>
							<div class="card-body--">
								<div class="table-stats order-table ov-h">
									<table id="mytable" class="table ">
										<thead>
											<tr>
												<th>ID</th>
												<th>Image</th>
												<th>Categories</th>
												<th>Added Date</th>
												<th>Modified Date/Time</th>
												<th>NO. of Products</th>
												<th>Order</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
											if (!empty($result)) {
												foreach ($result as $key => $value) {
											?>
													<tr>
														<td><?php echo $value['id'] ?></td>
														<td><img src="../media/category/<?php echo $value['v_image'] ?>" /></td>
														<td><?php echo $value['v_categories'] ?></td>
														<td><?php echo $value['dt_addon_date'] ?></td>
														<td><?php echo $value['dt_modified_date'] ?></td>
														<td><?php echo $value['total'] ?></td>
														<td><?php echo $value['i_order'] ?></td>
														<td>
															<?php
															if ($value['ti_status'] == 1) {
																echo "<a class='btn btn-success' href='?type=ti_status&operation=deactive&id=" . $value['id'] . "'>Active</a>";
															} else {
																echo "<a class='btn btn-warning' href='?type=ti_status&operation=active&id=" . $value['id'] . "'>Deactive</a>";
															}
															?>
														</td>
														<td>
															<?php
															echo "<a class='btn btn-primary' href='categoryForm.php?id=" . $value['id'] . "'>Edit</a>&nbsp;";
															echo "<a class='btn btn-danger' href='?type=delete&id=" . $value['id'] . "'>Delete</a>";
															?>
														</td>
													</tr>
											<?php
												}
											}
											?>
										</tbody>

									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<?php
require('../include/footer_2.inc.php');
?>