<?php
(isset($_GET['id']) && $_GET['id'] != '') ? $title = "Edit Category" : $title = "Add Category";
require('../include/top_2.inc.php');

$message = $msg = $imageErr = $updateData = $updateImg = '';

$catForm = new DB_con;

if (isset($_GET['id']) && $_GET['id'] != '') {
    $id = $_GET['id'];
    $result = $catForm->select('*', 'tbl_categories','', " AND id = '$id'");
    if (count($result)>0){
        foreach ($result as $key => $value){
        }
    }
}

if (isset($_POST['submit'])) {

    if (empty($_POST['name'])) {
        $message = "* Name required";
        header('Location:categoryForm.php?message=' . $message);
        exit;
    }

    if (empty($_POST['order'])) {
        $message = "* Order required";
        header('Location:categoryForm.php?message=' . $message);
        exit;
    }

    if (($_POST['status'] == '')) {
        $message = "* Status required";
        header('Location:categoryForm.php?message=' . $message);
        exit;
    }

    $categoryName = $_POST['name'];
    $order = $_POST['order'];
    $status = $_POST['status'];
    $image = $_FILES['image']['name'];
    move_uploaded_file($_FILES['image']['tmp_name'], '../media/category/' . $image);

    $file_ext = explode('.', $image);
    $file_ext_check = strtolower(end($file_ext));
    $allowed_types = ['jpg', 'png', 'jpeg', 'gif'];

    if (!isset($_GET['id'])) {
        if (empty($_FILES['image']['name'])) {
            $imageErr = "Please Select Image";
        }
        if (!empty($_FILES['image']['name'])) {
            if (!in_array($file_ext_check, $allowed_types)) {
                $imageErr = "file type is not allowed (only 'jpg', 'png', 'jpeg', 'gif' allowed)";
            }
        }
    } else {
        if (!empty($_FILES['image']['name'])) {
            if (!in_array($file_ext_check, $allowed_types)) {
                $imageErr = "file type is not allowed (only 'jpg', 'png', 'jpeg', 'gif' allowed)";
            }
        }
    }

    if (!$imageErr && !$message) {

        if (isset($_GET['id']) && $_GET['id'] != '') {
            $condition_arr = ['id' => $id, 'v_categories' => $categoryName, 'i_order' => $order, 'ti_status' => $status];
            $updateData = $catForm->updateData('tbl_categories', $condition_arr, 'id', $id);
         
            if ($_FILES['image']['error'] == 0) {
                $condition_arr = ['id' => $id, 'v_image' => $image];
                $updateImg = $catForm->updateData('tbl_categories', $condition_arr, 'id', $id);
                }
                if($updateData || $updateImg){
                    $_SESSION['alrt'] = "Data Updated Successfully!";
                    header('location:categories_listing.php');
                }  
        } else {
            $condition_arr = ['v_image' => $image, 'v_categories' => $categoryName, 'i_order' => $order, 'ti_status' => $status];
            $insertData =$catForm->insertData('tbl_categories', $condition_arr);
            if($insertData){
                $_SESSION['alrt'] = "Data Inserted Successfully!";
                header('location:categories_listing.php');
            }
        }
    }
}
?>

<div class="content pb-0">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <?php
                    if (isset($_GET['id']) && $_GET['id'] != '') { ?>
                        <div class="card-header"><strong>Edit Category</strong></div>
                    <?php } else { ?>
                        <div class="card-header"><strong>Add Category</strong></div>
                    <?php } ?>
                    <form method="post" enctype="multipart/form-data" autocomplete="off">
                        <div class="card-body card-block">

                            <div class="image-preview">
                                <img src="../media/category/<?php echo isset($value['v_image']) ? $value['v_image'] : ''  ?>" id="fileList" class="image-preview__image" />
                            </div>

                            <div class="form-group">
                                <label for="categories" class=" form-control-label">Category Image: <span class="star">*</span></label>
                                <input type="file" name="image" class="form-control" id="fileElem" onchange="preview(event)">
                                <span class="error"><?php echo $imageErr; ?> </span>
                            </div>

                            <div class="form-group">
                                <label for="categories" class=" form-control-label">Category Name: <span class="star">*</span></label>
                                <input type="text" name="name" placeholder="Enter Category" class="form-control" value="<?php echo isset($value['v_categories']) ? $value['v_categories'] : ''  ?>">
                            </div>

                            <div class="form-group">
                                <label for="categories" class=" form-control-label">Order: <span class="star">*</span></label>
                                <input type="text" name="order" placeholder="Enter Order" class="form-control" value="<?php echo isset($value['i_order']) ? $value['i_order'] : ''  ?>">
                            </div>

                            <div class="form-group">
                                <label for="categories" class=" form-control-label">Status: <span class="star">*</span></label>
                                <select class="form-control" name="status" value="<?php echo $status ?>">
                                    <option value="" selected>Status</option>
                                    <option value="1" <?php echo (isset($value['ti_status']) && $value['ti_status'] == 1)   ? "selected" : ""; ?>>Active</option>
                                    <option value="0" <?php echo (isset($value['ti_status']) && $value['ti_status'] == 0) ? "selected" : ""; ?>>Deactive</option>
                                </select>
                            </div>
                            <?php
                            if (isset($_GET['message'])) {
                            ?>
                                <div class="field_error"><?php echo $_GET['message'] ?></div>
                            <?php
                            }
                            ?>

                            <button name="submit" type="submit" class="btn btn-lg btn-info btn-block">
                                <span name="submit">Submit</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require('../include/footer_2.inc.php');
?>