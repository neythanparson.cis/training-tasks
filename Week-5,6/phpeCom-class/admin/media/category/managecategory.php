<?php 
    include "inc/header.php";
    include "classfunction.php";
?>
<?php 
    //insert record======================== add process========================
if(isset($_POST['save']))
{
    // Get the value from post method
    $filename =$_FILES["image"]["name"];
    $tempname = $_FILES["image"]["tmp_name"];
    $folder = "asset/img/".$filename;
    $name = $_POST["name"];
    $adt = date("Y-m-d H:i:s");
    $mdt = date("Y-m-d H:i:s");
    $order = $_POST["order"];
    $status = $_POST["status"];
    // image is set or not 
    if(isset($_FILES["image"]["name"]))
    {
        // allowed extension array
        $allowed = array("jpg" => "image/jpg", "png" =>"image/png","jpeg" => "image/jpeg");
        //fetch extension
        $extensn = pathinfo($filename, PATHINFO_EXTENSION);
        // check the allowed extension are exists or not
        if (!array_key_exists($extensn, $allowed))
        {
            //pop up box informing about valid extensions
            header("location:managecategory.php");
        }
        else
        {
            if(move_uploaded_file($tempname, $folder))//move image to folder
            {
                $values = array('v_image' => $filename,
                                'v_name' => $name,
                                'dt_added' => $adt,
                                'dt_modified' => $mdt,
                                'v_order1' => $order,
                                'v_status1' => $status);
                $data = new DatabaseConnection();
                $rowData = $data->insert("category", $values);
                header('location:managecategory.php?msg=add');
            }
        }
    }
}
//===========================update process==================
if(isset($_POST['update'])) //check submit button is set or net
{
        // Get the value from post method
        $filename = $_FILES["image"]["name"];
        $tempname = $_FILES["image"]["tmp_name"];
        $folder = "asset/img/".$filename;
        $name = $_POST["name"];
        $adt = $_SESSION['dta'];
        $mdt = date("Y-m-d H:i:s");
        $order = $_POST["order"];
        $status = $_POST["status"];
        $id = $_SESSION['id'];

        // image is set or not
        if(isset($_FILES["image"]["name"]))
        {
            //for empty file upload feild
            if($_FILES["image"]["name"] == "")
            {
                //if only data is updated without selecting new image then
                //old image is set and new received data is updated
                $filename = $_SESSION['img'];
                $data = new DatabaseConnection();
                $values = array(
                    'v_image' => $filename,
                    'dt_added' => $adt,
                    'v_name' => $name,
                    'dt_modified' => $mdt,
                    'v_order1' => $order,
                    'v_status1 ' => $status);
                $rowData = $data->update("category", $values, "AND i_cid= ".$id."");
                header("location:category-list.php?msg=update");
            }
            else
            {
                // allowed extension array
                $allowed = array("jpg" => "image/jpg", "png" =>"image/png","jpeg" => "image/jpeg");

                //fetch extension
                $extensn = pathinfo($filename, PATHINFO_EXTENSION);

                // check the allowed are exists or not
                if (!array_key_exists($extensn, $allowed))
                {
                    //redirect to given location and pass the id to give user pop up msg.
                    header("location:managecategory.php?id=fail");
                }
                else
                {
                    if(move_uploaded_file($tempname, $folder))//move image to folder
                    {
                        $data = new DatabaseConnection();
                        $values = array(
                            'v_image' => $filename,
                            'dt_added' => $adt,
                            'v_name' => $name,
                            'dt_modified' => $mdt,
                            'v_order1' => $order,
                            'v_status1 ' => $status);
                        $rowData = $data->update("category", $values, "AND i_cid= ".$id."");
                        header("location:category-list.php?msg=update");
                    }
                }
            }
        }
}
  //=================================data add edit===================

if(isset($_GET['action']))
{ 
    if(($_GET['action'] == 'add') || ($_GET['action'] == "edit"))
    {
        if (isset($_GET['id1'])) // check id is set or not 
        {
?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                    <!-- Content Header (Page header) -->
                        <section class="content-header">
                            <ol class="breadcrumb">
                                <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
                                <li class="active"><a href="managecategory.php">Manage Category</a></li>
                                <li>Update Category</li>
                            </ol>
                        </section>
            </aside>
            <aside class="right-side">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-danger">
                                <div class="box-header">
                                    <h1 style="text-align:left;font-family:'Times New Roman', Times, serif;background-color:lightpink;">Update Category</h1>
                                </div>
                                <div class="box">
                                    <div class="box-body no-padding">
                                        <!-- form start -->
                                            <?php
                                                $a="SELECT * from category where i_cid = '".$_GET['id1']."' ";
                                                $query=mysqli_query($con,$a);
                                                if($row=mysqli_fetch_assoc($query))
                                                {
                                                    $_SESSION['id']=$row['i_cid'];
                                                    $_SESSION['dta'] =$row['dt_added'];
                                                    $_SESSION['img'] = $row['v_image'];

                                            ?>
                                                <form role="form" id= "#forms" method="post"
                                                    action="managecategory.php" enctype="multipart/form-data">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                                <!-- name feild -->
                                                            <label>Name : </label>
                                                            <input type="text" class="form-control" name="name"
                                                            value= "<?php echo $row['v_name']; ?>"
                                                            data-parsley-minlength="3"
                                                            data-parsley-maxlength="10"
                                                            required data-parsley-pattern="^[a-zA-Z]+$"
                                                            data-parsley-trigger="change"
                                                            required>
                                                        </div>
                                                        <div class="form-group">
                                                            <img height="100" width="100"
                                                            src="asset/img/<?php echo $row['v_image']; ?>">
                                                        </div>
                                                        <div class="form-group">
                                                                <!-- Upload image -->
                                                            <label for="exampleInputFile">Upload Image : </label>
                                                            <input type="file" name="image" id="exampleInputFile"
                                                            value = "<?php echo $row['v_image']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                                    <!-- order feild-->
                                                            <label>Order : </label>
                                                            <input type="text" name="order" class="form-control"
                                                            value= "<?php echo $row['v_order1']; ?>"
                                                            data-parsley-type="integer"
                                                            data-parsley-trigger="change"
                                                            data-parsley-min="1"
                                                            required>
                                                        </div>
                                                                    <!-- select -->
                                                        <div class="form-group">
                                                            <label>Select</label>
                                                            <select class="form-control" name="status">
                                                                <option value = 1>Active  </option>
                                                                <option value = 0>Inactive</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <input class="btn btn-primary" type="submit" name="update" value= "update">
                                                        </div>
                                                    </div>
                                                </form>
                                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pull-left">
                                &nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href="managecategory.php">Go Back</a>
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            <script type="text/javascript">
            $(function () {
                $('#forms').parsley();
            });
            </script>
<?php
        }
        else
        {
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="managecategory.php">Manage Category</a></li>
            <li>Add Category</li>
        </ol>
    </section>
</aside>
<aside class="right-side">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header">
                        <h1 style="text-align:left;font-family:'Times New Roman', Times, serif;background-color:lightpink;">Add Category</h1>
                    </div>
                            <!-- form start -->
                        <form role="form" id= "#form" method="post" data-parsley-validate
                            action="managecategory.php" enctype="multipart/form-data">
                            <div class="box-body table-responsive no-padding">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <!-- Upload image -->
                                        <label for="exampleInputFile">Upload Image : </label>
                                        <input type="file" name="image" id="exampleInputFile" required>
                                    </div>
                                    <div class="form-group">
                                            <!-- name feild -->
                                        <label>Name : </label>
                                        <input type="text" class="form-control" name="name"
                                        placeholder="Enter Name" data-parsley-minlength="3"
                                        data-parsley-maxlength="10" required data-parsley-pattern="^[a-zA-Z]+$" 
                                        data-parsley-trigger="change" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                            <!-- order feild-->
                                        <label>Order : </label>
                                        <input type="text" name="order" class="form-control"
                                        data-parsley-type="integer" data-parsley-trigger="change"
                                        data-parsley-min="1" placeholder="Enter Order" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Select Status</label>
                                        <select class="form-control" name="status">
                                            <option value = 1>Active  </option>
                                            <option value = 0>Inactive</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input class="btn btn-primary" type="submit" id="btnSubmit" 
                                        name="save" value= "submit">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
                <a class="btn btn-primary" href="managecategory.php">Go Back</a>
        </div>
    </section>
</aside><!-- /.right-side -->
<script type="text/javascript">
    $(function () {
        $('#form').parsley();
    });
</script>
<?php
        }
    }
}
 else
 {
 ?>
        <!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
        <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="managecategory.php">Manage Category</a></li>
            <li>View Category</li>
        </ol>
    </section>
</aside>
<aside class="right-side">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header">
                        <h1 style="text-align:left;font-family:'Times New Roman', Times, serif;background-color:lightpink;"> Categories</h1>
                        <?php
                                if(isset($_GET['msg']))
                                {
                                    if($_GET['msg'] == 'add')
                                    {
                            ?>
                            <div class="alert alert-success alert-dismissable col-md-4 pull-right">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Alert ! </b> Data Added successfully..
                            </div>
                            <?php
                                    }
                                    else if($_GET['msg'] == 'fail')
                                    {
                            ?>
                            <div class="alert alert-warning alert-dismissable col-md-4 pull-right">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Alert ! </b> Name And Image should be unique. JPEG / JPG/ PNG files are allowed..
                            </div>
                            <?php
                                    }
                                    else if($_GET['msg'] == 'reject')
                                    {
                            ?>
                            <div class="alert alert-warning alert-dismissable col-md-4 pull-right">
                                <i class="fa fa-warning"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Alert ! </b> This category has a product..
                            </div>
                            <?php
                                    }
                                    else if($_GET['msg'] == 'update')
                                    {
                            ?>
                            <div class="alert alert-info alert-dismissable col-md-4 pull-right">
                                <i class="fa fa-info"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Alert !  </b>  Data Updated Successfully..
                                </div>
                            <?php
                                    }
                                    else if($_GET['msg'] == 'delete')
                                    {
                            ?>
                            <div class="alert alert-danger alert-dismissable col-md-4 pull-right">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Alert ! </b> Record Deleted Successfully..
                            </div>
                            <?php
                                    }
                                }
                            ?>
                    </div>
                        <div class="box">
                            <div class="box-body no-padding">
                                <table class="table table-striped" style="font-size: 20px;font-family:Times New Roman">
                                    <tr>
                                        <td>
                                                <!-- link to add new category in to table -->
                                            &nbsp;&nbsp;&nbsp;<a class="btn btn-primary"
                                            href="managecategory.php?action=add"> Add Category </a>
                                            <!-- search form -->
                                        </td>
                                            <form action="managecategory.php" method="post" class="sidebar-form">
                                                <div  class="input-group">
                                                    <td>
                                                        <input type="text" name="search" class="form-control" 
                                                        placeholder="Search" required>
                                                    </td>
                                                    <td>
                                                        <input type='submit' name='sbtn' id='search-btn' class="btn btn-primary" 
                                                        class="btn btn-flat" value="search">
                                                    </td>
                                                </div>
                                            </form>
                                    </tr>
                                    <tr>
                                        <th>Id</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Added Date</th>
                                        <th>Modified Date</th>
                                        <th>Order</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                        <?php
                                            /* Get total number of records */
                                            if (isset($_GET['pageno']))
                                            {
                                                $pageno = $_GET['pageno'];
                                            }
                                            else {
                                                $pageno = 1;
                                            }
                                                $no_of_records_per_page = 4;
                                                $offset = ($pageno-1) * $no_of_records_per_page;

                                                $conn=$con;
                                                $total_pages_sql = "SELECT COUNT(*) FROM category";
                                                $result = mysqli_query($conn,$total_pages_sql);
                                                $total_rows = mysqli_fetch_array($result)[0];
                                                $total_pages = ceil($total_rows / $no_of_records_per_page);

                                                $count=0;
                                                        // code to display record of retrived data for search
                                                if (isset($_POST['sbtn'])) // check search btn is set or not
                                                {
                                                    $name = $_POST['search'];
                                                    if($name != NULL)
                                                    {
                                                        $sql="SELECT * from category where
                                                        (`v_name` LIKE '%".$name."%') OR (`v_status1` LIKE '%".$name."%') ";
                                                        $res_data = mysqli_query($conn,$sql); 
                                                    }
                                                }
                                                else
                                                {
                                                    $sql = "SELECT * FROM category LIMIT $offset, $no_of_records_per_page";
                                                    $res_data = mysqli_query($conn,$sql); 
                                                }
                                                while($row=mysqli_fetch_array($res_data))
                                                { $count++;
                                        ?>
                                                <!-- diisplay the fetched records into table using obj $row-->
                                    <tr id="<?php echo $row['i_cid'] ?>">
                                            <td><?php echo $count; ?></td>
                                            <td><img height="100" width="100"
                                                src="asset/img/<?php echo $row['v_image']; ?>"></td>
                                            <td><?php echo $row['v_name']; ?></td>
                                            <td><?php echo $row['dt_added']; ?></td>
                                            <td><?php echo $row['dt_modified']; ?></td>
                                            <td><?php echo $row['v_order1']; ?></td>
                                            <td><?php 
                                                            if($row['v_status1'] == 0)
                                                            {
                                                                echo "Inactive";
                                                            } 
                                                            else{
                                                                echo "Active";
                                                            }
                                                ?></td>
                                            <td><button type="button" class="btn btn-danger deletebtn">
                                                <i class='glyphicon glyphicon-trash'></i></button>
                                            <a href = "managecategory.php?action=edit&id1=<?php echo $row['i_cid']; ?>" 
                                            class="btn btn-danger"> 
                                            <i class="fa fa-edit"></i> </a>
                                            </td>
                                    </tr>
                                        <?php
                                                }
                                                if($count == 0)
                                                {
                                                    echo "<tr><td><b>Data Not Available</b></td></tr>";
                                                }
                                                
                                        ?>
                                </table>
                                <div id="deletemodal" class="modal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Delete Record</h4>
                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to delete this record ?</p>
                                                    <form method="POST" action="managecategory.php" id="form-delete-user">
                                                        <input type="hidden" name="deleteid" id="deleteid">
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" form="form-delete-user" name="deletedata" class="btn btn-danger">Delete</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                
                                    <div class="pull-right">
                                    <!----================list of pagination===========================----------------------->
                                        <ul class="pagination">
                                            <li><a href="?pageno=1">First</a></li>
                                            <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
                                                <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Prev</a>
                                            </li>
                                            <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
                                                <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Next</a>
                                            </li>
                                            <li><a href="?pageno=<?php echo $total_pages; ?>">Last</a></li>
                                        </ul>
                                </div><!----- list end --->
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    if(isset($_POST['sbtn']))
                    {
                        echo "<a class='btn btn-primary' href='manageproduct.php'>Go Back </a>";
                    }
                ?>
            </div> 
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->

<?php
    } // view dta end 
?>
<script>
$(document).ready(function () {
    $('.deletebtn').on('click',function(){
        $('#deletemodal').modal('show');
            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function () {
                return $(this).text();
                
            }).get();
            console.log(data);
            $('#deleteid').val(data[0]);
    });
    
});
</script>
<?php 
    if(isset($_POST['deleteid']))
    {
        $sql = "DELETE FROM category WHERE i_cid = '".$_POST['deleteid']."'";
        $result = mysqli_query($con,$sql);
        header("location:managecategory.php?msg=delete");
        if(!$result)
        {
            header("location:managecategory.php?msg=reject");  
        }
    }
?>