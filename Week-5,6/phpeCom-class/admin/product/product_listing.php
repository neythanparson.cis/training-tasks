<?php
$title = "Product";
require('../include/top_2.inc.php');
$msg = $str = $minPrice = $maxPrice = $minQty = $maxQty = '';

$proList = new DB_con;

//STATUS (ACTIVE/DEACTIVE)
if (isset($_GET['type']) && $_GET['type'] != '') {
	$type = $_GET['type'];
	if ($type == 'ti_status') {
		$operation = $_GET['operation'];
		$id = $_GET['id'];
		($operation == 'active') ? $status = '1' : $status = '0';
		$condition_arr = ['ti_status' => $status];
		$proList->updateData('tbl_product', $condition_arr, 'id', $id);
	}
}

//Delete
if (isset($_GET['type']) &&  $_GET['type'] != '') {
	$type = $_GET['type'];
	if ($type == 'delete') {
		$id = $_GET['id'];
		$condition_arr = ['id' => $id];
		$proList->deleteData('tbl_product', $condition_arr);
	}
}
if (isset($_POST['search'])) {

	$str = $_POST['product-search'];
	$minPrice = $_POST['min-price'];
	$maxPrice = $_POST['max-price'];

	$minQty = $_POST['min-qty'];
	$maxQty = $_POST['max-qty'];

	$statusSearch = $_POST['status'];

	$fields = "tbl_product.id,tbl_product.v_name,tbl_product.i_product_code,tbl_product.f_price,tbl_product.f_sale_price,tbl_product.i_qty,tbl_product.dt_added_on,tbl_product.dt_modified_on,tbl_product.i_order,tbl_product.ti_status,tbl_categories.v_categories,tbl_product_img.v_image";
	$leftJoin= "LEFT JOIN tbl_categories ON tbl_product.v_categories=tbl_categories.id LEFT JOIN tbl_product_img ON tbl_product.id=tbl_product_img.i_product_id";

	if (!empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && !empty($_POST['status'])) {
		$search = ['tbl_product.v_name' => "LIKE '%$str%'", 'tbl_product.f_price' => "BETWEEN '$minPrice' AND '$maxPrice'", 'tbl_product.i_qty' => "BETWEEN '$minQty' AND '$maxQty'", 'tbl_product.ti_status' => "= $statusSearch"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (!empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && empty($_POST['min-qty']) && empty($_POST['max-qty']) && empty($_POST['status'])) {
		$search = ['tbl_product.v_name' => "LIKE '%$str%'"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (!empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && empty($_POST['min-qty']) && empty($_POST['max-qty']) && empty($_POST['status'])) {
		$search = ['tbl_product.v_name' => "LIKE '%$str%'", 'tbl_product.f_price' => "BETWEEN '$minPrice' AND '$maxPrice'"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (!empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && empty($_POST['status'])) {
		$search = ['tbl_product.v_name' => "LIKE '%$str%'", 'tbl_product.f_price' => "BETWEEN '$minPrice' AND '$maxPrice'", 'tbl_product.i_qty' => "BETWEEN '$minQty' AND '$maxQty'"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (!empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && empty($_POST['status'])) {
		$search = ['tbl_product.v_name' => "LIKE '%$str%'", 'tbl_product.i_qty' => "BETWEEN '$minQty' AND '$maxQty'"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (!empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && !empty($_POST['status'])) {
		$search = ['tbl_product.v_name' => "LIKE '%$str%'", 'tbl_product.i_qty' => "BETWEEN '$minQty' AND '$maxQty'", 'tbl_product.ti_status' => "= $statusSearch"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (!empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && empty($_POST['min-qty']) && empty($_POST['max-qty']) && !empty($_POST['status'])) {
		$search = ['tbl_product.v_name' => "LIKE '%$str%'", 'tbl_product.ti_status' => "= $statusSearch"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && !empty($_POST['status'])) {
		$search = ['tbl_product.f_price' => "BETWEEN '$minPrice' AND '$maxPrice'", 'tbl_product.i_qty' => "BETWEEN '$minQty' AND '$maxQty'", 'tbl_product.ti_status' => "= $statusSearch"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && empty($_POST['status'])) {
		$search = ['tbl_product.f_price' => "BETWEEN '$minPrice' AND '$maxPrice'", 'tbl_product.i_qty' => "BETWEEN '$minQty' AND '$maxQty'"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && empty($_POST['min-qty']) && empty($_POST['max-qty']) && empty($_POST['status'])) {
		$search = ['tbl_product.f_price' => "BETWEEN '$minPrice' AND '$maxPrice'"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && empty($_POST['min-qty']) && empty($_POST['max-qty']) && !empty($_POST['status'])) {
		$search = ['tbl_product.f_price' => "BETWEEN '$minPrice' AND '$maxPrice'", 'tbl_product.ti_status' => "= $statusSearch"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && empty($_POST['status'])) {
		$search = ['tbl_product.i_qty' => "BETWEEN '$minQty' AND '$maxQty'"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && empty($_POST['min-qty']) && empty($_POST['max-qty']) && !empty($_POST['status'])) {
		$search = ['tbl_product.ti_status' => "= $statusSearch"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	}

	elseif (empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && !empty($_POST['status'])) {
		$search = ['tbl_product.i_qty' => "BETWEEN '$minQty' AND '$maxQty'", 'tbl_product.ti_status' => "= $statusSearch"];
		$result = $proList->select($fields , ' tbl_product ', $leftJoin , "AND tbl_product_img.i_main_image='1'", $search);
	} 
	else {
		$result = $proList->select('tbl_product.id,tbl_product.v_name,tbl_product.i_product_code,tbl_product.f_price,tbl_product.f_sale_price,tbl_product.i_qty,tbl_product.dt_added_on,tbl_product.dt_modified_on,tbl_product.i_order,tbl_product.ti_status,tbl_categories.v_categories,tbl_product_img.v_image', 'tbl_product', ' LEFT JOIN tbl_categories ON tbl_product.v_categories=tbl_categories.id LEFT JOIN tbl_product_img ON tbl_product.id=tbl_product_img.i_product_id', "AND tbl_product_img.i_main_image='1'");
	}
}
if (!isset($_POST['search'])) {
	$result = $proList->select('tbl_product.id,tbl_product.v_name,tbl_product.i_product_code,tbl_product.f_price,tbl_product.f_sale_price,tbl_product.i_qty,tbl_product.dt_added_on,tbl_product.dt_modified_on,tbl_product.i_order,tbl_product.ti_status,tbl_categories.v_categories,tbl_product_img.v_image', 'tbl_product', ' LEFT JOIN tbl_categories ON tbl_product.v_categories=tbl_categories.id LEFT JOIN tbl_product_img ON tbl_product.id=tbl_product_img.i_product_id', "AND tbl_product_img.i_main_image='1'");
}

?>

<form action="" method="post">
	<div class="content pb-0">
		<div class="orders">
			<div class="row ">
				<div class="col-xl-12 grid-margin stretch-card">
					<form id="change">
						<div class="row">
							<a href="productForm.php" type="button" class=" btn btn-primary ml-3 "><i class="fa fa-plus" aria-hidden="true">&nbsp;&nbsp;</i> Add New Record!</a>

							<input type="text" name="product-search" placeholder="Product Name" class="form-control col-2 ml-5" value="<?php echo $str ?>">

							<label for="" class="ml-2 mt-2">Price-Range:</label>
							<input type="text" name="min-price" placeholder="Min-price" class="form-control col-1 ml-1" value="<?php echo $minPrice  ?>">
							<input type="text" name="max-price" placeholder="Max-price" class="form-control col-1" value="<?php echo $maxPrice ?>">

							<label for="" class="ml-2 mt-2">Qty-Range:</label>
							<input type="text" name="min-qty" placeholder="Min-qty" class="form-control col-1 ml-1" value="<?php echo $minQty ?>">
							<input type="text" name="max-qty" placeholder="Max-qty" class="form-control col-1" value="<?php echo $maxQty ?>">

							<select class="form-control col-1 ml-2" id="status" name="status">
								<option value="" selected>Status</option>
								<option value="'1'" <?php echo (isset($_POST['status']) && $_POST['status'] == "'1'") ? 'selected' : '' ?>>Active</option>
								<option value="'0'" <?php echo (isset($_POST['status']) && $_POST['status'] == "'0'") ? 'selected' : '' ?>>Deactive</option>
							</select>

							<input type="submit" value="Search" name="search" class="btn btn-primary ml-2 "></input>
							<input type="submit" id="reset" value="Reset" name="reset" class="btn btn-danger ml-2 "></input>

						</div>
					</form>
					<div class="field_error"><?php echo $msg ?></div>
					<div class="card ">
						<div class="card-body">
						<?php
						if (isset($_SESSION['alrt'])) {
						?>
							<div class="alert alert-success alert-dismissible fade show" role="alert">
								<strong>Bro! </strong><?php echo $_SESSION['alrt']; ?>
							</div>
						<?php
							unset($_SESSION['alrt']);
						}
						?>
							<h4 class="box-title mb-3">Product</h4>
							<div class="card-body--">
								<div class="table-stats order-table ov-h">
									<table id="mytable" class="table ">
										<thead>
											<tr>
												<th>ID</th>
												<th>Image</th>
												<th>Name</th>
												<th>Product Code</th>
												<th>Category name</th>
												<th>Price</th>
												<th>Sale_price</th>
												<th>Qty</th>
												<th>Added Date</th>
												<th>Modified Date/Time</th>
												<th>Order</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
											if (!empty($result)) {
												foreach ($result as $key => $value) { ?>
													<tr>
														<td><?php echo $value['id'] ?></td>
														<td><img src="../media/product/<?php echo $value['v_image'] ?>" /></td>
														<td><?php echo $value['v_name'] ?></td>
														<td><?php echo $value['i_product_code'] ?></td>
														<td><?php echo $value['v_categories'] ?></td>
														<td><?php echo $value['f_price'] ?></td>
														<td><?php echo $value['f_sale_price'] ?></td>
														<td><?php echo $value['i_qty'] ?></td>
														<td><?php echo $value['dt_added_on'] ?></td>
														<td><?php echo $value['dt_modified_on'] ?></td>
														<td><?php echo $value['i_order'] ?></td>
														<td>
															<?php
															if ($value['ti_status'] == 1) {
																echo "<a class='btn btn-success' href='?type=ti_status&operation=deactive&id=" . $value['id'] . "'>Active</a>";
															} else {
																echo "<a class='btn btn-warning' href='?type=ti_status&operation=active&id=" . $value['id'] . "'>Deactive</a>";
															}
															?>
														</td>
														<td>
															<?php
															echo "<a class='btn btn-primary' href='productForm.php?id=" . $value['id'] . "'>Edit</a>&nbsp;";
															echo "<a class='btn btn-danger' href='?type=delete&id=" . $value['id'] . "'>Delete</a>";
															?>
														</td>
													</tr>
											<?php
												}
											}
											?>
										</tbody>

									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<?php
require('../include/footer_2.inc.php');
?>