<?php
(isset($_GET['id']) && $_GET['id'] != '') ? $title = "Edit Product" : $title = "Add Product";
require('../include/top_2.inc.php');
$imageErr = $categoriesErr =  $productnameErr  = $priceErr = $salepriceErr = $qtyErr = $orderErr = $statusErr = "";

$msg = '';
$proForm = new DB_con;

//Fetch data from table to form
if (isset($_GET['id']) && $_GET['id'] != '') {
    $id = $_GET['id'];

    $getImageData = $proForm->select('*', 'tbl_product_img','', "AND i_product_id=" . $_GET['id']);

    $res = $proForm->select('tbl_product.id,tbl_product.v_name,tbl_product.i_product_code,tbl_product.f_price,tbl_product.f_sale_price,tbl_product.i_qty,tbl_product.i_order,tbl_product.ti_status,tbl_categories.v_categories,tbl_categories.id', 'tbl_product', ' LEFT JOIN tbl_categories ON tbl_product.v_categories=tbl_categories.id ', "AND tbl_product.id='$id'");

    if (count($res) > 0) {
        foreach ($res as $productData);
        $categoryid = $productData['id'];
    }

}

if (isset($_POST['submit'])) {
    //get data from form to table and database
    if (isset($_GET['id']) && $_GET['id'] != '') {
        $selectedMainImage =  $_POST['selectedMainImage'];
        $productID = $_POST['productID'];
    }
    $categoryName = $_POST['category'];
    $productname = $_POST['productname'];
    $productcode = mt_rand(1, 10000);
    $price = $_POST['price'];
    $saleprice = $_POST['saleprice'];
    $qty = $_POST['qty'];
    $order = $_POST['order'];
    $status = $_POST['status'];

    $image = $_FILES['image']['name'];
    $allowed_types = ['jpg', 'png', 'jpeg', 'gif'];

    if (($_POST["category"] == 0)) {
        $categoriesErr = "Category is required.";
    }

    if (empty($_POST["productname"])) {
        $productnameErr = "Please enter Name.";
    }

    if (empty($_POST["price"])) {
        $priceErr = "Please enter Price.";
    } else {
        $price= $proForm->input_data($_POST["price"]);
        if (!preg_match("/^[0-9]*$/", $price)) {
            $priceErr = "Only numeric value is allowed.";
        }
    }

    if (empty($_POST["saleprice"])) {
        $salepriceErr = "Please enter Saleprice.";
    } else {
        $saleprice = $proForm->input_data($_POST["saleprice"]);
        if (!preg_match("/^[0-9]*$/", $saleprice)) {
            $salepriceErr = "Only numeric value is allowed.";
        }
    }

    if (empty($_POST["qty"])) {
        $qtyErr = "Please enter Quantity.";
    } else {
        $qty = $proForm->input_data($_POST["qty"]);
        if (!preg_match("/^[0-9]*$/", $qty)) {
            $qtyErr = "Only numeric value is allowed.";
        }
    }

    if (empty($_POST["order"])) {
        $orderErr = "Please enter Orders.";
    } else {
        $order = $proForm->input_data($_POST["order"]);
        if (!preg_match("/^[0-9]*$/", $order)) {
            $orderErr = "Only numeric value is allowed.";
        }
    }

    if (($_POST["status"] == '')) {
        $statusErr = "Status is required.";
    }
    if (!$categoriesErr && !$productnameErr  && !$priceErr && !$salepriceErr && !$qtyErr && !$orderErr && !$statusErr) {

        if (isset($_GET['id']) && $_GET['id'] != '') {

            if ($_FILES['image']['error'][0] == 0) {

                $countfiles = count($image);
                $allowed_types = ['jpg', 'png', 'jpeg', 'gif'];
                for ($i = 0; $i < $countfiles; $i++) {

                    $image = $_FILES['image']['name'][$i];
                    $file_ext = pathinfo($image, PATHINFO_EXTENSION);

                    if (in_array(strtolower($file_ext), $allowed_types)) {
                        move_uploaded_file($_FILES['image']['tmp_name'][$i], '../media/product/' . $image);

                        if ($_FILES['image']['error'][0] == 0) {
                            $condition_arr = ['i_product_id' => $id, 'v_image' => $image, 'i_main_image' => '0'];
                            $img = $proForm->insertData('tbl_product_img', $condition_arr);
                        }
                        $condition_arr = ['i_product_id' => $productID, 'i_main_image' => '0'];
                        $imgOld = $proForm->updateData('tbl_product_img', $condition_arr, 'i_product_id', $productID);

                        $condition_arr = ['i_product_id' => $id, 'i_main_image' => '1'];
                        $imgNew = $proForm->updateData('tbl_product_img', $condition_arr, 'id', $selectedMainImage);

                        $condition_arr = ['v_categories' => $categoryName, 'v_name' => $productname, 'f_price' => $price, 'f_sale_price' => $saleprice, 'i_qty' => $qty, 'i_order' => $order, 'ti_status' => $status];
                        $dataUpdate = $proForm->updateData('tbl_product', $condition_arr, 'id', $id);
                    } else {
                        $imageErr = "file type is not allowed (only 'jpg', 'png', 'jpeg', 'gif' allowed)";
                    }
                }
            } else {
                $condition_arr = ['i_product_id' => $productID, 'i_main_image' => '0'];
                $imgOld = $proForm->updateData('tbl_product_img', $condition_arr, 'i_product_id', $productID);

                $condition_arr = ['i_product_id' => $id, 'i_main_image' => '1'];
                $imgNew = $proForm->updateData('tbl_product_img', $condition_arr, 'id', $selectedMainImage);

                $condition_arr = ['v_categories' => $categoryName, 'v_name' => $productname, 'f_price' => $price, 'f_sale_price' => $saleprice, 'i_qty' => $qty, 'i_order' => $order, 'ti_status' => $status];
                $dataUpdate = $proForm->updateData('tbl_product', $condition_arr, 'id', $id);
            }
            if($imgOld || $imgNew || $dataUpdate ){
                $_SESSION['alrt'] = "Data Updated Successfully!";
                header('location:product_listing.php');
            }  
        } else {
            $condition_arr = ['v_name' => $productname, 'v_categories' => $categoryName, 'i_product_code' => $productcode, 'f_price' => $price, 'f_sale_price' => $saleprice, 'i_qty' => $qty, 'i_order' => $order, 'ti_status' => $status];
            $insert = $proForm->insertData('tbl_product', $condition_arr);

            $countfiles = count($image);
            for ($i = 0; $i < $countfiles; $i++) {

                $filename = $_FILES['image']['name'][$i];
                $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (in_array(strtolower($file_ext), $allowed_types)) {

                    move_uploaded_file($_FILES['image']['tmp_name'][$i], '../media/product/' . $filename);
                    $i == 0 ? $condition_arr = ['i_product_id' => $insert, 'v_image' => $filename, 'i_main_image' => '1'] : $condition_arr = ['i_product_id' => $insert, 'v_image' => $filename, 'i_main_image' => '0'];
                    $imageUploads = $proForm->insertData('tbl_product_img', $condition_arr);
                } else {
                    $imageErr =  "* file type is not allowed (only 'jpg', 'png', 'jpeg', 'gif' allowed)";
                }
            }
            if($imageUploads){
                $_SESSION['alrt'] = "Data Inserted Successfully!";
                header('location:product_listing.php');
            }
        }
    }
}
?>

<div class="content pb-0">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <?php
                    if (isset($_GET['id']) && $_GET['id'] != '') { ?>
                        <div class="card-header"><strong>Edit Product</strong></div>
                    <?php } else { ?>
                        <div class="card-header"><strong>Add Product</strong></div>
                    <?php } ?>
                    <form method="post" enctype="multipart/form-data" autocomplete="off">
                        <div class="card-body card-block">
                            <div id="image_preview">
                                <?php
                                if (isset($_GET['id']) && $_GET['id'] != '') {
                                    if (!empty($getImageData)) {
                                        foreach ($getImageData as $key => $images) {
                                ?>
                                            <input type="hidden" value="<?php echo $images['i_product_id'] ? $images['i_product_id'] : '' ?>" name="productID">

                                            <!-- radio btn on active img -->
                                            <input type="radio" name="selectedMainImage" value="<?php echo $images['id']; ?>" <?php echo $images['i_main_image'] == 1 ? "checked" : "" ?>>

                                            <!-- delete btn on inactive img  -->
                                            <?php if ($images['i_main_image'] == 0) { ?>
                                                <a class='btn btn-danger' href='../delete_image.php?type=delete&id=<?php echo $images['id'] . '&product_id=' . $images['i_product_id'] ?>'>Delete</a>
                                            <?php } ?>

                                            <!-- dynamic img preview -->
                                            <img src="../media/product/<?php echo $images['v_image']; ?>" style='width: 100px; height: 90px; border: 2px solid #dddddd; margin:5px; padding:10px'>
                                <?php
                                        }
                                    }
                                }
                                ?>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Categories: <span class="star">*</span></label>
                                <select name="category" class="form-control">
                                    <?php
                                    if (isset($_GET['id']) && $_GET['id'] != '') {
                                    ?>
                                        <option value="<?php echo $productData['id']; ?>" <?php echo $categoryid == $productData['id'] ? "selected" : "" ?>><?php echo $productData['v_categories']; ?></option>
                                        <?php
                                        $res = $proForm->select('id,v_categories', 'tbl_categories', '', " AND `ti_status` = '1'");
                                        foreach ($res as $key) {
                                            echo "<option value=" . $key['id'] . ">" . $key['v_categories'] . "</option>";
                                        }
                                        ?>
                                    <?php } else { ?>
                                        <option value="0" selected>Select Categories</option>
                                    <?php
                                        $res = $proForm->select('id,v_categories', 'tbl_categories', '', " AND `ti_status` = '1'");
                                        foreach ($res as $key) {
                                            echo "<option value=" . $key['id'] . ">" . $key['v_categories'] . "</option>";
                                        }
                                    }
                                    ?>
                                </select>
                                <span class="error"><?php echo $categoriesErr; ?> </span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Product Image: </label>
                                <!-- populate data in textbox -->
                                <input type="file" name="image[]" class="form-control" id="upload_file" multiple onchange="preview_image(event)">
                                <span class="field_error"><?php echo $imageErr ?></span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Product Name: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="productname" placeholder="Enter Product Name" class="form-control" value="<?php echo isset($productData['v_name']) ? $productData['v_name'] : ''  ?>">
                                <span class="error"><?php echo $productnameErr; ?> </span>

                            </div>

                            <?php
                            if (isset($_GET['id']) && $_GET['id'] != '') { ?>
                                <div class="form-group">
                                    <label for="product" class=" form-control-label">Product Code: </label>
                                    <!-- populate data in textbox -->
                                    <input type="text" name="productcode" placeholder="Enter Product Code" value="<?php echo isset($productData['i_product_code']) ? $productData['i_product_code'] : ''  ?>" class="form-control" tabindex=-1 readonly="readonly ">
                                </div>
                            <?php } ?>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Price: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="price" placeholder="Enter Product Price" class="form-control" value="<?php echo isset($productData['f_price']) ? $productData['f_price'] : ''  ?>">
                                <span class="error"><?php echo $priceErr; ?> </span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Sale Price: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="saleprice" placeholder="Enter Product Sale Price" class="form-control" value="<?php echo isset($productData['f_sale_price']) ? $productData['f_sale_price'] : ''  ?>">
                                <span class="error"><?php echo $salepriceErr; ?> </span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Quantity: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="qty" placeholder="Enter Quantity" class="form-control" value="<?php echo isset($productData['i_qty']) ? $productData['i_qty'] : ''  ?>">
                                <span class="error"><?php echo $qtyErr; ?> </span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Order: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="order" placeholder="Enter Order" class="form-control" value="<?php echo isset($productData['i_order']) ? $productData['i_order'] : ''  ?>">
                                <span class="error"><?php echo $orderErr; ?> </span>
                            </div>


                            <div class="form-group">
                                <label for="product" class=" form-control-label">Status: <span class="star">*</span></label>
                                <select class="form-control" name="status">
                                    <option value="" selected>Status</option>
                                    <option value="1" <?php echo (isset($productData['ti_status']) && $productData['ti_status'] == 1)   ? "selected" : ""; ?>>Active</option>
                                    <option value="0" <?php echo (isset($productData['ti_status']) && $productData['ti_status'] == 0) ? "selected" : ""; ?>>Deactive</option>
                                </select>
                                <span class="error"><?php echo $statusErr; ?> </span>
                            </div>

                            <button name="submit" type="submit" class="btn btn-lg btn-info btn-block">
                                <span name="submit">Submit</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require('../include/footer_2.inc.php');
?>