<?php
session_start();
if($_SESSION['ADMIN_LOGIN']){
    $_SESSION['alrt'] = "Logged Out Successfully!";
}
// The unset() function unsets a variable.
unset($_SESSION['ADMIN_LOGIN']);
unset($_SESSION['ADMIN_USERNAME']);

//will redirect to the login page 
header('location:login.php');  
die();
?>