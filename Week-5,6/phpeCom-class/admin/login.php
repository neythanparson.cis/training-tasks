<?php
include('include/class.inc.php');
$message = '';
$msg = '';

$login = new DB_con();

if (isset($_POST['submit'])) {
   if (empty($_POST['username'])) {
      $message = "Username required";
      header('Location:login.php?message=' . $message);
      exit;
   }

   if (empty($_POST['password'])) {
      $message = "Password required";
      header('Location:login.php?message=' . $message);
      exit;
   }
   $username = $_POST['username'];
   $password = md5($_POST['password']);

   $sql = $login->select('*', 'tbl_admin_users','',"AND v_username = '$username' AND v_password = '$password'");
   

   if ($sql[0]['v_username'] == $username && $sql[0]['v_password'] == $password) {
      $_SESSION['ADMIN_LOGIN'] = 'yes';
      $_SESSION['ADMIN_USERNAME'] = $username;
      $_SESSION['ADMIN_PASSWORD'] = $password;
   } 
   if($_SESSION['ADMIN_LOGIN']){
      $_SESSION['alrt'] = "";
      header('location:index.php');
      die;
  }
   else {
      $message = "Please Enter Correct Details";
      header('Location:login.php?message=' . $message);
      exit;
   }
}

?>
<!doctype html>
<html class="no-js" lang="">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Admin login</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="assets/css/normalize.css">
   <link rel="stylesheet" href="assets/css/bootstrap.min.css">
   <link rel="stylesheet" href="assets/css/font-awesome.min.css">
   <link rel="stylesheet" href="assets/css/themify-icons.css">
   <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">
   <link rel="stylesheet" href="assets/css/flag-icon.min.css">
   <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
   <link rel="stylesheet" href="assets/css/style.css">
   <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body style="background-image: url(././images/bg.jpg);background-size: 100%;">
   <div class="sufee-login d-flex align-content-center flex-wrap">
      <div class="container">
         <div class="login-content">
            <div class="login-form mt-150">
            <?php
						if (isset($_SESSION['alrt'])) {
						?>
							<div class="alert alert-warning alert-dismissible fade show" role="alert">
								<strong>Bro! </strong><?php echo $_SESSION['alrt']; ?>
							</div>
						<?php
							unset($_SESSION['alrt']);
						}
						?>
               <form method="post" autocomplete="off">
                  <div style="text-align:center;font-size:x-large"><strong>Admin Login</strong></div>
                  <hr class="my-3">
                  <?php
                  if (isset($_GET['message'])) {
                  ?>
                     <div class="field_error"><?php echo $_GET['message'] ?></div>
                  <?php
                  }
                  ?>
                  <div class="form-group">
                     <label><strong>Username</strong></label>
                     <input type="text" name="username" class="form-control" placeholder="Username">
                  </div>
                  <div class="form-group">
                     <label><strong>Password</strong></label>
                     <input type="password" name="password" class="form-control" placeholder="Password">
                  </div>
                  <button type="submit" name="submit" class="btn btn-primary mb-30 mt-30">Login</button>
               </form>
            </div>
         </div>
      </div>
   </div>
   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
   <script src="assets/js/vendor/jquery-2.1.4.min.js" type="text/javascript"></script>
   <script src="assets/js/popper.min.js" type="text/javascript"></script>
   <script src="assets/js/plugins.js" type="text/javascript"></script>
   <script src="assets/js/main.js" type="text/javascript"></script>
   <script>
   $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-dismissible").alert('close');
});
</script>
</body>

</html>