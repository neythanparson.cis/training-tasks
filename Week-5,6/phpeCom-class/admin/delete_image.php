<?php 
require('include/class.inc.php');

$delete_image = new DB_con;

if (isset($_GET['id'])) 
{
    $imageID = $_GET['id'];
    $productID= $_GET['product_id'];
    
    $condition_arr=['id'=>$imageID];
    $delete=$delete_image->deleteData('tbl_product_img',$condition_arr); 
    header("location:product/productForm.php?id=$productID");
}     
?>