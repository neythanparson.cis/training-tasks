<?php
require('connection.inc.php');
  // Initialize message variable
  $msg = "";

  // If upload button is clicked ...
  if (isset($_POST['upload'])) {
  	// Get image name
  	$image = $_FILES['image']['name'];
	// $_FILES function returns array which includes[name],[type],[tmp_name],[error],[size] 
	//   echo "<pre>";
	//   print_r($_FILES);
	//   exit;

  	// image file directory
  	$target = "images/".basename($image);
	  //concate with the folder path
	  // will return name of selected file "ex:-images/watches_PNG101451.png"

  	$sql = "INSERT INTO tbl_images (v_image) VALUES ('$image')";
  	// execute query
  	mysqli_query($con, $sql);

  	if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
	// move_uploaded_files :- movses an uploaded file to a new location
  		$msg = "Image uploaded successfully";
  	}else{
  		$msg = "Failed to upload image";
  	}
  }
  $result = mysqli_query($con, "SELECT * FROM tbl_images");

?>																
<!DOCTYPE html>
<html>
<head>
<title>Image Upload</title>
<style type="text/css">
   #content{
   	width: 100%;
   	margin: 20px auto;
   	border: 1px solid #cbcbcb;
   }
   form{
   	width: 50%;
   	margin: 20px auto;
   }
   form div{
   	margin-top: 5px;
   }
   img{
   	float: left;
   	margin: 5px;
   	width: 300px;
   	height: 250px;
   }
</style>
</head>
<body>
<form method="POST" action="index.php" enctype="multipart/form-data">
  	<input type="hidden" name="size" value="1000000">
  	<div>
  	  <input type="file" name="image">
  	</div>
   
  	<div>
  		<button type="submit" name="upload">Upload</button>
  	</div>
  </form>
<div id="content">
<?php
    while ($row = mysqli_fetch_array($result)) {
		//mysqli_fetch_array :- fetches array with id and name
      	echo "<img src='images/".$row['v_image']."'>";
		//   print_r($row);
		// 	exit;
    }
  ?>
</div>
</body>
</html>