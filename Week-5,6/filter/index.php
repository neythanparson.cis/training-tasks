<!DOCTYPE html>
<html lang="en">
<head>
  <title>Filter Tutorial on Technopoints</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
  <style>
 
/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media screen and (max-width: 500px) {
    .column {
        width: 100%;
    }
}
.thumbnail img{
    max-width: 100%; /* do not stretch the bootstrap column */
}

.img-wrapper{
	width: 100%;
	padding-bottom: 100%; /* your aspect ratio here! */
	position: relative;
}

.img-wrapper img{
	position: absolute;
	top: 0; 
	bottom: 0; 
	left: 0; 
	right: 0;
	min-height: 50%;
	max-height: 100%;
	min-width:100%/* optional: if you want the smallest images to fill the .thumbnail */
}
</style>
</head>
<body>

<div class="container">
  <h2>Technopoints: Filter</h2>
  <p>From below dropdown menus you can apply filter</p>
  <form action="" method="post">
    <div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <select class="form-control" name="category">
                <option value="">Select Category</option>
                <option value="fiction">Science Fiction</option>
                <option value="Mystery">Mystery</option>
				<option value="Adventure">Adventure</option>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <select class="form-control" name="year">
                <option value ="">Year of Publishing</option>
                <option value="2016">2016</option>
                <option value="2017">2017</option>
				<option value="2018">2018</option>
            </select>
        </div>
    </div>
	    <div class="col-sm-3">
        <div class="form-group">
            <select class="form-control" name="price">
                <option value="">Select Price Range</option>
                <option value="range1">01$ - 05$</option>
                <option value="range2">05$ - 10$</option>
				<option value="range3">10$ - 15$</option>
            </select>
        </div>
    </div>
	<button type="submit" class="btn btn-primary">Apply</button>
</div>
</form>
</div>
<center><h2>Apply Filter To Check Result</h2><br><center>
<div class="container-fluid">
    <div class="row"><?php
         error_reporting( error_reporting() & ~E_NOTICE );
	 if(isset($_POST['price'])) {	if($_POST['price']=='range1'){
			$low = 1; $high = 5;
		}
		if($_POST['price']=='range2'){
			$low = 5; $high = 10;
		}
		if($_POST['price']=='range3'){
			$low = 10; $high = 15;
		} }
		@$category = $_POST['category'];
		@$year = $_POST['year'];
		//@$price = $_POST['price'];
		include 'dbconfig.php';
		if(isset($_POST['price']) && isset($_POST['category']) && isset($_POST['year'])) { $qry = "SELECT * FROM filter
		WHERE price BETWEEN $low AND $high AND year = $year AND category ='$category'"; }
		if(isset($_POST['price']) && isset($_POST['category']) && $_POST['year']==NULL){
			$qry = "SELECT * FROM filter
		WHERE price BETWEEN $low AND $high AND category ='$category'";
		}
		if(isset($_POST['category']) && isset($_POST['year']) && $_POST['price']==NULL){
			$qry = "SELECT * FROM filter
		WHERE year = $year AND category ='$category'";
		}
		if(isset($_POST['price']) && isset($_POST['year']) && $_POST['category']==NULL){
			$qry = "SELECT * FROM filter
		WHERE price BETWEEN $low AND $high AND year = $year";
		}
		if(isset($_POST['category']) && $_POST['year']==NULL && $_POST['price']==NULL){
			$qry = "SELECT * FROM filter
		WHERE category ='$category'";
		}
		if($_POST['category']==NULL && isset($_POST['year']) && $_POST['price']==NULL){
			$qry = "SELECT * FROM filter
		WHERE year =$year";
		}
		if($_POST['category']==NULL && $_POST['year']==NULL && isset($_POST['price'])){
			$qry = "SELECT * FROM filter
		WHERE price BETWEEN $low AND $high";
		}
		if($_POST['category']==NULL && $_POST['year']==NULL && $_POST['price']==NULL){
			$qry = "SELECT * FROM filter";
		}
		$result = mysqli_query($con,$qry);
		$num = mysqli_num_rows($result);
		  if($num > 0) while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){ ?>
        <!-- item -->
        <div class="col-xs-4 col-sm-2">
            <div class="thumbnail">
                <div class="img-wrapper">
                    <img src="cover/<?php echo $row['cover'];?>" alt="400">
                </div>
                <div class="caption text-center">Title:<?php echo $row['title'];?></div><!-- optional -->
				<div class="caption text-center">Publishing Year:<?php echo $row['year'];?></div>
				<div class="caption text-center">Category:<?php echo $row['category'];?></div>
				<div class="caption text-center">Price:<?php echo $row['price'].'$';?></div>
				
				
			</div>
        </div>
           <?php }else{
			   echo "<h4>No Result Found</h4>"; } ?>
</div>
</div>
</body>
</html>