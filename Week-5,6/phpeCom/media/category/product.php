<!-- The default username for MySQL database server is root and there is no password. However to prevent your databases from intrusion and unauthorized access you should set password for MySQL accounts. --><!doctype html>
<html class="no-js" lang="">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Categories</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="assets/css/normalize.css">
   <link rel="stylesheet" href="assets/css/bootstrap.min.css">
   <link rel="stylesheet" href="assets/css/font-awesome.min.css">
   <link rel="stylesheet" href="assets/css/themify-icons.css">
   <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">
   <link rel="stylesheet" href="assets/css/flag-icon.min.css">
   <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
   <link rel="stylesheet" href="assets/css/style.css">
   <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>

<body>
   <aside id="left-panel" class="left-panel">
      <nav class="navbar navbar-expand-sm navbar-default">
         <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
               <li class="menu-title">Menu</li>
               <li class="menu-item-has-children dropdown">
                  <a href="categories.php"> Categories</a>
               </li>
               <li class="menu-item-has-children dropdown">
                  <a href="product.php"> Product</a>
               </li>
            </ul>
         </div>
      </nav>
   </aside>
   <div id="right-panel" class="right-panel">
      <header id="header" class="header">
         <div class="top-left">
            <div class="navbar-header">
               <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="Logo"></a>
               <a class="navbar-brand hidden" href="index.php"><img src="images/logo2.png" alt="Logo"></a>
               <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
            </div>
         </div>
         <div class="top-right">
            <div class="header-menu">
               <div class="user-area dropdown float-right">
                  <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Welcome Admin</a>
                  <div class="user-menu dropdown-menu">
                     <a class="nav-link" href="logout.php"><img class="wc mb-1" style="height: 35px; width: 35px; border-top-left-radius: 50% 50%; border-top-right-radius: 50% 50%; border-bottom-right-radius: 50% 50%; border-bottom-left-radius: 50% 50%;" src=././images/chalta.jpg>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Logout</a>
                     <!-- <i class="fa fa-power-off"> -->
                  </div>
               </div>
            </div>
         </div>
      </header>
<form action="" method="post">
	<div class="content pb-0">
		<div class="orders">
			<div class="row ">
				<div class="col-xl-12 grid-margin stretch-card">
					<form action="" id="change">
						<!-- <form class="form-inline "> -->
						<a href="add_product.php" type="button" class=" btn btn-primary  mb-3"><i class="fa fa-plus" aria-hidden="true">&nbsp;&nbsp;</i> Add New Record!</a>
						<div class="float-right form-inline ">

							<!-- Category-name filter -->
							<input type="text" name="product-search" placeholder="Product Name" class="form-control mb-3" value="">



							<!-- Price range filter -->
							<select class="form-control  ml-2 mb-3" name="price">
								<option value="" selected>Select Price Range</option>
								<option value="range1">Low - Medium</option>
								<option value="range2">Medium - High</option>
								<option value="range3">High - Very High</option>
							</select>

							<!-- Quantity range filter  -->
							<select class="form-control  ml-2 mb-3" name="qty">
								<option value="" selected>Select Qty Range</option>
								<option value="qty1">1 - 500</option>
								<option value="qty2">500 - 1000</option>
								<option value="qty3">1000 - 100000</option>
							</select>

							<!-- Status filter -->
							<select class="form-control  ml-2 mb-3" name="status">
								<option value="" selected>Status</option>
								<option value="'1'" >Active</option>
								<option value="'0'" >Deactive</option>
							</select>

							<input type="submit" value="Search" name="search" class="btn btn-primary ml-2 mb-3"></input>
							<!-- Reset form -->
							<input type="submit" value="Reset" name="reset" class="btn btn-danger ml-2 mb-3"></input>

						</div>
					</form>
					<div class="field_error"></div>

					<!-- </form> -->

					<div class="card ">
						<div class="card-body">
							<h4 class="box-title mb-3">Product</h4>
							<div class="card-body--">
								<div class="table-stats order-table ov-h">
									<table class="table ">
										<thead>
											<tr>
												<th class="serial">#</th>
												<th>ID</th>
												<th>Image</th>
												<th>Name</th>
												<th>Product Code</th>
												<th>Category name</th>
												<th>Price</th>
												<th>Sale_price</th>
												<th>Qty</th>
												<th>Added Date</th>
												<th>Modified Date/Time</th>
												<th>Order</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
																								<tr>
														<td>1</td>
														<td>3</td>
														<td><img src="../media/product/women_bag_PNG6428.png" /></td>
														<td>Jenna Rich</td>
														<td>9747</td>
														<td>Bags-new</td>
														<td>608</td>
														<td>435</td>
														<td>693</td>
														<td>2022-03-28</td>
														<td>2022-03-28 18:18:14</td>
														<td>12</td>
														<td>
															<a class='btn btn-success' href='?type=ti_status&operation=deactive&id=3'>Active</a>														</td>
														<td>
															<a class='btn btn-primary' href='edit_product.php?id=3'>Edit</a>&nbsp;<a class='btn btn-danger' href='?type=delete&id=3'>Delete</a>														</td>
													</tr>
																								<tr>
														<td>2</td>
														<td>2</td>
														<td><img src="../media/product/pngegg.png" /></td>
														<td>Ella Adams</td>
														<td>6132</td>
														<td>Shoes</td>
														<td>653</td>
														<td>816</td>
														<td>74</td>
														<td>2022-03-28</td>
														<td>2022-03-28 18:18:16</td>
														<td>14</td>
														<td>
															<a class='btn btn-success' href='?type=ti_status&operation=deactive&id=2'>Active</a>														</td>
														<td>
															<a class='btn btn-primary' href='edit_product.php?id=2'>Edit</a>&nbsp;<a class='btn btn-danger' href='?type=delete&id=2'>Delete</a>														</td>
													</tr>
																								<tr>
														<td>3</td>
														<td>1</td>
														<td><img src="../media/product/—Pngtree—gold wedding rings 3d illustration_5390186.png" /></td>
														<td>Byron Washington</td>
														<td>9365</td>
														<td>Rings</td>
														<td>250</td>
														<td>300</td>
														<td>738</td>
														<td>2022-03-28</td>
														<td>2022-03-28 18:18:17</td>
														<td>12</td>
														<td>
															<a class='btn btn-success' href='?type=ti_status&operation=deactive&id=1'>Active</a>														</td>
														<td>
															<a class='btn btn-primary' href='edit_product.php?id=1'>Edit</a>&nbsp;<a class='btn btn-danger' href='?type=delete&id=1'>Delete</a>														</td>
													</tr>
																					</tbody>

									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>




<div class="clearfix"></div>
         <footer class="site-footer">
            <div class="footer-inner bg-white" >
               <div class="row">
                  <div class="col-sm-6">
                     Copyright &copy; 2021-2022 Nikhil Patel
                  </div>
               </div>
            </div>
         </footer>
      </div>
      <script src="assets/js/vendor/jquery-2.1.4.min.js" type="text/javascript"></script>
      <script src="assets/js/popper.min.js" type="text/javascript"></script>
      <script src="assets/js/plugins.js" type="text/javascript"></script>
      <script src="assets/js/main.js" type="text/javascript"></script>
      <script src="assets/js/custom.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>



   </body>
</html>