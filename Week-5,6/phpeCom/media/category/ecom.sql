-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2022 at 07:13 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin_users`
--

CREATE TABLE `tbl_admin_users` (
  `id` int(11) NOT NULL,
  `v_username` varchar(255) NOT NULL,
  `v_password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin_users`
--

INSERT INTO `tbl_admin_users` (`id`, `v_username`, `v_password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE `tbl_categories` (
  `id` int(11) NOT NULL,
  `v_image` varchar(155) DEFAULT NULL,
  `v_categories` varchar(255) NOT NULL,
  `dt_addon_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `dt_modified_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `i_order` int(11) DEFAULT NULL,
  `i_no_of_products` int(11) DEFAULT NULL,
  `ti_status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`id`, `v_image`, `v_categories`, `dt_addon_date`, `dt_modified_date`, `i_order`, `i_no_of_products`, `ti_status`) VALUES
(1, 'pngegg.png', 'Shoes', '2022-04-04 04:38:33', '2022-04-04 04:38:33', 13, 0, 1),
(2, '—Pngtree—gold wedding rings 3d illustration_5390186.png', 'Rings', '2022-04-04 04:38:49', '2022-04-04 04:38:49', 100, 0, 1),
(3, 'women_bag_PNG6428.png', 'Bags', '2022-04-04 04:39:17', '2022-04-04 04:39:17', 30, 0, 1),
(4, 'watches_PNG101451.png', 'Watch', '2022-04-04 04:39:38', '2022-04-04 04:39:49', 200, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL,
  `v_name` varchar(255) NOT NULL,
  `v_categories` int(11) NOT NULL,
  `i_product_code` int(11) NOT NULL,
  `f_price` float NOT NULL,
  `f_sale_price` float NOT NULL,
  `i_qty` int(11) NOT NULL,
  `dt_added_on` date NOT NULL DEFAULT current_timestamp(),
  `dt_modified_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `i_order` int(11) NOT NULL,
  `ti_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `v_name`, `v_categories`, `i_product_code`, `f_price`, `f_sale_price`, `i_qty`, `dt_added_on`, `dt_modified_on`, `i_order`, `ti_status`) VALUES
(1, 'Nathaniel Peterson', 3, 8807, 450, 427, 426, '2022-04-04', '2022-04-04 04:40:17', 100, 1),
(2, 'Ray Austin', 4, 7625, 17, 916, 699, '2022-04-04', '2022-04-04 04:40:30', 120, 1),
(3, 'Kimberly Murray', 1, 1749, 840, 127, 376, '2022-04-04', '2022-04-04 04:40:45', 30, 1),
(4, 'Patrick Davenport', 2, 4844, 95, 938, 518, '2022-04-04', '2022-04-04 04:41:00', 40, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_img`
--

CREATE TABLE `tbl_product_img` (
  `id` int(11) NOT NULL,
  `i_product_id` int(11) NOT NULL,
  `v_image` varchar(255) NOT NULL,
  `i_main_image` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_product_img`
--

INSERT INTO `tbl_product_img` (`id`, `i_product_id`, `v_image`, `i_main_image`) VALUES
(1, 1, 'women_bag_PNG6428.png', '1'),
(2, 2, 'watches_PNG101451.png', '1'),
(3, 3, 'pngegg.png', '1'),
(4, 4, '—Pngtree—gold wedding rings 3d illustration_5390186.png', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin_users`
--
ALTER TABLE `tbl_admin_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product_img`
--
ALTER TABLE `tbl_product_img`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin_users`
--
ALTER TABLE `tbl_admin_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_product_img`
--
ALTER TABLE `tbl_product_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
