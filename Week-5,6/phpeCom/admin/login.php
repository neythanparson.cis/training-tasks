<?php
require('connection.inc.php');
require('functions.inc.php');
//initial value of msg is null
$msg = '';
// isset( $_POST['submit'] ) : This line checks if the form is submitted using the isset() function, but works only if the form input type submit has a name attribute (name="submit").

if (isset($_POST['submit'])) {
   $username=$_POST['username'];
   $password=md5($_POST['password']);
   //called Function
   // Procedural style / will escape special char. if any
   
   $username = get_safe_value($con, $_POST['username']);
   $password = get_safe_value($con, md5($_POST['password']));
   // echo 'password :'.' '.$password;
   // echo '<br>';
   // echo 'username :'.' '.$username;
   // echo '<pre>';
   // print_r($_POST);
   // exit;
   //OP:- password : 21232f297a57a5a743894a0e4a801fc3
   //     username : admin
   // Array
   // (
   //     [username] => admin
   //     [password] => 21232f297a57a5a743894a0e4a801fc3
   //     [submit] => 
   // )

   //binary is used for casesensitive input
   $sql = "select * from tbl_admin_users where binary v_username='$username' and v_password='$password'";
   // print_r($sql);
   // exit;
   // OP:-select * from tbl_admin_users where binary v_username='admin' and v_password='21232f297a57a5a743894a0e4a801fc3'


   // The query() / mysqli_query() function performs a query against a database.
   $res = mysqli_query($con, $sql);
   // echo "<pre>";
   // print_r($res);
   // exit;
   //OP:- mysqli_result Object
   // (
   //    [current_field] => 0
   //    [field_count] => 3
   //    [lengths] => 
   //    [num_rows] => 1
   //    [type] => 0
   // ) 

   // The mysqli_num_rows() function returns the number of rows in a result set
   $count = mysqli_num_rows($res);
   // echo $count;
   // exit;
   //OP:- 1
   if ($count == 1) {
      //will allow user to login
      $_SESSION['ADMIN_LOGIN'] = 'no';
      //session variables are set eg:'no'
      // Session variables solve this problem by storing user information to be used across multiple pages (e.g. username, favorite color, etc). By default, session variables last until the user closes the browser. So; Session variables hold information about one single user, and are available to all pages in one application.
      $_SESSION['ADMIN_USERNAME'] = $username;
      $_SESSION['ADMIN_PASSWORD'] = md5($password);
      // print_r($_SESSION);
      // exit;
      //OP:- Array ( [ADMIN_LOGIN] => no [ADMIN_USERNAME] => admin [ADMIN_PASSWORD] => c3284d0f94606de1fd2af172aba15bf3  )


      //will redirect to index page 
      header('location:index.php');
      die();
   } else {
      $msg = "Please enter correct login details";
   }
}
?>
<!doctype html>
<html class="no-js" lang="">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Admin login</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="assets/css/normalize.css">
   <link rel="stylesheet" href="assets/css/bootstrap.min.css">
   <link rel="stylesheet" href="assets/css/font-awesome.min.css">
   <link rel="stylesheet" href="assets/css/themify-icons.css">
   <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">
   <link rel="stylesheet" href="assets/css/flag-icon.min.css">
   <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
   <link rel="stylesheet" href="assets/css/style.css">
   <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body style="background-image: url(././images/bg.jpg);background-size: 100%;">
   <div class="sufee-login d-flex align-content-center flex-wrap">
      <div class="container">
         <div class="login-content">
            <div class="login-form mt-150">
               <form method="post" autocomplete="off">
                  <div style="text-align:center;font-size:x-large"><strong>Admin Login</strong></div>
                  <hr class="my-3">
                  <div class="form-group">
                     <label><strong>Username</strong></label>
                     <input type="text" name="username" class="form-control" placeholder="Username">
                  </div>
                  <div class="form-group">
                     <label><strong>Password</strong></label>
                     <input type="password" name="password" class="form-control" placeholder="Password">
                  </div>
                  <button type="submit" name="submit" class="btn btn-primary mb-30 mt-30">Login</button>
               </form>
               <div class="field_error"><?php echo $msg ?></div>
            </div>
         </div>
      </div>
   </div>
   <script src="assets/js/vendor/jquery-2.1.4.min.js" type="text/javascript"></script>
   <script src="assets/js/popper.min.js" type="text/javascript"></script>
   <script src="assets/js/plugins.js" type="text/javascript"></script>
   <script src="assets/js/main.js" type="text/javascript"></script>
</body>

</html>
