<?php
if (isset($_GET['id']) && $_GET['id'] != '') {
    $title = "Edit Product";
} else {
    $title = "Add Product";
}
require('top.inc.php');

$imageErr = $categoriesErr =  $productnameErr  = $priceErr = $salepriceErr = $qtyErr = $orderErr = $statusErr = "";

//function for validation
function input_data($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}


//Fetch data from table to form
if (isset($_GET['id']) && $_GET['id'] != '') {
    $id = get_safe_value($con, $_GET['id']);

    //query to get product data from product table
    $res = mysqli_query($con, "SELECT tbl_product.id,tbl_product.v_name,tbl_product.i_product_code,tbl_product.f_price,tbl_product.f_sale_price,tbl_product.i_qty,tbl_product.i_order,tbl_product.ti_status,tbl_categories.v_categories,tbl_categories.id FROM tbl_product LEFT JOIN tbl_categories ON tbl_product.v_categories=tbl_categories.id  WHERE tbl_product.id='$id'");

    //query to get image data from image table 
    $productImageRes = mysqli_query($con, "SELECT * FROM tbl_product_img  WHERE i_product_id='$id'");

    $check = mysqli_num_rows($res);
    if ($check > 0) {

        $productData = mysqli_fetch_assoc($res);
        //defined category i'd and status to get back value of thm
        $categoryid = $productData['id'];

        //get all image data which has same product i'd.
        $imgArr = [];
        while ($row = mysqli_fetch_assoc($productImageRes)) {
            $imgArr[] = $row;
        }
    } else {
        header('location:product.php');
        die();
    }
}


//Submit Edited data
if (isset($_POST['submit'])) {

    if (isset($_GET['id']) && $_GET['id'] != '') {
        $selectedMainImage = get_safe_value($con, $_POST['selectedMainImage']);
        $productID = get_safe_value($con, $_POST['productID']);
    }
    $categories = get_safe_value($con, $_POST['category_id']);
    $productcode = mt_rand(1, 10000);
    $productname = get_safe_value($con, $_POST['productname']);
    $price = get_safe_value($con, $_POST['price']);
    $saleprice = get_safe_value($con, $_POST['saleprice']);
    $qty = get_safe_value($con, $_POST['qty']);
    $order = get_safe_value($con, $_POST['order']);
    $status = get_safe_value($con, $_POST['status']);

    $image = $_FILES['image']['name'];

    //validation
    //category validation
    if (($_POST["category_id"] == 0)) {
        $categoriesErr = "Category is required.";
    }

    //name validate
    if (empty($_POST["productname"])) {
        $productnameErr = "Please enter Name.";
    }

    //price validation
    if (empty($_POST["price"])) {
        $priceErr = "Please enter Price.";
    } else {
        $price = input_data($_POST["price"]);
        // check if price only contains numbers.  
        if (!preg_match("/^[0-9]*$/", $price)) {
            $priceErr = "Only numeric value is allowed.";
        }
    }

    //sale price validation
    if (empty($_POST["saleprice"])) {
        $salepriceErr = "Please enter Saleprice.";
    } else {
        $saleprice = input_data($_POST["saleprice"]);
        // check if price only contains numbers.  
        if (!preg_match("/^[0-9]*$/", $saleprice)) {
            $salepriceErr = "Only numeric value is allowed.";
        }
    }

    //Qty validation
    if (empty($_POST["qty"])) {
        $qtyErr = "Please enter Quantity.";
    } else {
        $qty = input_data($_POST["qty"]);
        // check if price only contains numbers.  
        if (!preg_match("/^[0-9]*$/", $qty)) {
            $qtyErr = "Only numeric value is allowed.";
        }
    }

    //Orders validation
    if (empty($_POST["order"])) {
        $orderErr = "Please enter Orders.";
    } else {
        $order = input_data($_POST["order"]);
        // check if price only contains numbers.  
        if (!preg_match("/^[0-9]*$/", $order)) {
            $orderErr = "Only numeric value is allowed.";
        }
    }

    if (($_POST["status"] == '')) {
        $statusErr = "Status is required.";
    }

    if (!$categoriesErr && !$productnameErr  && !$priceErr && !$salepriceErr && !$qtyErr && !$orderErr && !$statusErr) {

        if (isset($_GET['id']) && $_GET['id'] != '') {
            if ($_FILES['image']['error'][0] == 0) {
                $countfiles = count($image);
                $allowed_types = ['jpg', 'png', 'jpeg', 'gif'];
                for ($i = 0; $i < $countfiles; $i++) {

                    $image = $_FILES['image']['name'][$i];
                    $file_ext = pathinfo($image, PATHINFO_EXTENSION);

                    if (in_array(strtolower($file_ext), $allowed_types)) {

                        move_uploaded_file($_FILES['image']['tmp_name'][$i], '../media/product/' . $image);

                        if ($_FILES['image']['error'][0] == 0) {
                            $img = "INSERT INTO `tbl_product_img` (`i_product_id`, `v_image`,`i_main_image`) VALUES ('$id','$image','0')";
                            $result = mysqli_query($con, $img);
                        }
                        $dataUpdate = "UPDATE  tbl_product SET v_categories='$categories', v_name='$productname', f_price='$price',f_sale_price='$saleprice',i_qty='$qty',i_order='$order',ti_status='$status' WHERE id= '$id'";
                        $result = mysqli_query($con, $dataUpdate);


                        $imgOld = "UPDATE `tbl_product_img` SET `i_product_id`='$productID',`i_main_image`= '0' WHERE `i_product_id`= $productID";
                        $result = mysqli_query($con, $imgOld);

                        $imgNew = "UPDATE `tbl_product_img` SET `i_product_id`='$id',`i_main_image`= '1' WHERE `id`='$selectedMainImage'";
                        $result = mysqli_query($con, $imgNew);
                    } else {
                        $imageErr = "file type is not allowed (only 'jpg', 'png', 'jpeg', 'gif' allowed)";
                    }
                }
            } else {
                $dataUpdate = "UPDATE  tbl_product SET v_categories='$categories', v_name='$productname', f_price='$price',f_sale_price='$saleprice',i_qty='$qty',i_order='$order',ti_status='$status' WHERE id= '$id'";
                $result = mysqli_query($con, $dataUpdate);


                $imgOld = "UPDATE `tbl_product_img` SET `i_product_id`='$productID',`i_main_image`= '0' WHERE `i_product_id`= $productID";
                $result = mysqli_query($con, $imgOld);

                $imgNew = "UPDATE `tbl_product_img` SET `i_product_id`='$id',`i_main_image`= '1' WHERE `id`='$selectedMainImage'";
                $result = mysqli_query($con, $imgNew);
            }
        } else {

            $productQuery = "INSERT INTO`tbl_product`( `v_name`, `v_categories`, `i_product_code`, `f_price`, `f_sale_price`, `i_qty`, `dt_added_on`, `dt_modified_on`, `i_order`, `ti_status`) VALUES ('$productname','$categories','$productcode','$price','$saleprice','$qty',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'$order','$status')";
            if (mysqli_query($con, $productQuery)) {
                $last_id = mysqli_insert_id($con);
            }
            // Count total files
            $countfiles = count($image);
            $allowed_types = ['jpg', 'png', 'jpeg', 'gif'];
            // Looping all files
            for ($i = 0; $i < $countfiles; $i++) {

                $filename = $_FILES['image']['name'][$i];
                $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (in_array(strtolower($file_ext), $allowed_types)) {
                    move_uploaded_file($_FILES['image']['tmp_name'][$i], '../media/product/' . $filename);
                    if ($i == 0) {
                        $imageUploads = "INSERT INTO `tbl_product_img` (`i_product_id`, `v_image`,`i_main_image`) VALUES ('$last_id','$filename','1')";
                    } else {
                        $imageUploads = "INSERT INTO`tbl_product_img`(`i_product_id`,`v_image`) VALUES  ('$last_id','$filename')";
                    }
                    $result = mysqli_query($con, $imageUploads);
                } else {
                    $imageErr = "file type is not allowed (only 'jpg', 'png', 'jpeg', 'gif' allowed)";
                }
            }
        }
        if (isset($result)) {
            header("location:product.php");
        }
    }
}

?>
<div class="content pb-0">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <?php
                    if (isset($_GET['id']) && $_GET['id'] != '') { ?>
                        <div class="card-header"><strong>Edit Product</strong></div>
                    <?php } else { ?>
                        <div class="card-header"><strong>Add Product</strong></div>
                    <?php } ?>

                    <form method="post" enctype="multipart/form-data" autocomplete="off">


                        <div class="card-body card-block">


                            <div id="image_preview">
                                <?php
                                if (isset($_GET['id']) && $_GET['id'] != '') {
                                    foreach ($imgArr as $key => $value) {
                                ?>
                                        <input type="hidden" value="<?php echo isset($value['i_product_id']) ? $value['i_product_id'] : '' ?>" name="productID">

                                        <!-- radio btn on active img -->
                                        <input type="radio" name="selectedMainImage" value="<?php echo $value['id']; ?>" <?php echo $value['i_main_image'] == 1 ? "checked" : "" ?>>

                                        <!-- delete btn on inactive img  -->
                                        <?php if ($value['i_main_image'] == 0) { ?>
                                            <a class='btn btn-danger' href='delete_image.php?type=delete&id=<?php echo $value['id'] . '&product_id=' . $value['i_product_id'] ?>'>Delete</a>
                                        <?php } ?>

                                        <!-- dynamic img preview -->
                                        <img src="../media/product/<?php echo  $value['v_image']; ?>" style='width: 100px; height: 90px; border: 2px solid #dddddd; margin:5px; padding:10px'>

                                <?php
                                    }
                                }
                                ?>
                            </div>


                            <div class="form-group">
                                <label for="product" class=" form-control-label">Categories: <span class="star">*</span></label>
                                <!-- populate data in textbox -->

                                <select name="category_id" class="form-control">
                                    <?php
                                    if (isset($_GET['id']) && $_GET['id'] != '') {
                                    ?>
                                        <option value="<?php echo $productData['id']; ?>" <?php echo $categoryid == $productData['id'] ? "selected" : "" ?>><?php echo $productData['v_categories']; ?></option>
                                        <?php
                                        $res = mysqli_query($con, "SELECT id,v_categories FROM tbl_categories WHERE ti_status = 1 ORDER BY v_categories ASC");
                                        while ($row = mysqli_fetch_assoc($res)) {
                                            echo "<option value=" . $row['id'] . ">" . $row['v_categories'] . "</option>";
                                        }
                                        ?>
                                    <?php } else { ?>
                                        <option value="0" selected>Select Categories</option>
                                    <?php
                                        $res = mysqli_query($con, "SELECT id,v_categories FROM tbl_categories WHERE ti_status = 1 ORDER BY v_categories ASC");
                                        while ($row = mysqli_fetch_assoc($res)) {
                                            echo "<option value=" . $row['id'] . ">" . $row['v_categories'] . "</option>";
                                        }
                                    }
                                    ?>

                                </select>
                                <span class="error"><?php echo $categoriesErr; ?> </span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Product Image: </label>
                                <!-- populate data in textbox -->
                                <input type="file" name="image[]" class="form-control" id="upload_file" multiple onchange="preview_image(event)">
                                <span class="field_error"><?php echo $imageErr ?></span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Product Name: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="productname" placeholder="Enter Product Name" class="form-control" value="<?php echo isset($productData['v_name']) ? $productData['v_name'] : ''  ?>">
                                <span class="error"><?php echo $productnameErr; ?> </span>

                            </div>
                            
                            <?php
                            if (isset($_GET['id']) && $_GET['id'] != '') { ?>
                                <div class="form-group">
                                    <label for="product" class=" form-control-label">Product Code: </label>
                                    <!-- populate data in textbox -->
                                    <input type="text" name="productcode" placeholder="Enter Product Code" value="<?php echo isset($productData['i_product_code']) ? $productData['i_product_code'] : ''  ?>" class="form-control" tabindex=-1 readonly="readonly ">
                                </div>
                            <?php } ?>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Price: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="price" placeholder="Enter Product Price" class="form-control" value="<?php echo isset($productData['f_price']) ? $productData['f_price'] : ''  ?>">
                                <span class="error"><?php echo $priceErr; ?> </span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Sale Price: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="saleprice" placeholder="Enter Product Sale Price" class="form-control" value="<?php echo isset($productData['f_sale_price']) ? $productData['f_sale_price'] : ''  ?>">
                                <span class="error"><?php echo $salepriceErr; ?> </span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Quantity: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="qty" placeholder="Enter Quantity" class="form-control" value="<?php echo isset($productData['i_qty']) ? $productData['i_qty'] : ''  ?>">
                                <span class="error"><?php echo $qtyErr; ?> </span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Order: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="order" placeholder="Enter Order" class="form-control" value="<?php echo isset($productData['i_order']) ? $productData['i_order'] : ''  ?>">
                                <span class="error"><?php echo $orderErr; ?> </span>
                            </div>


                            <div class="form-group">
                                <label for="product" class=" form-control-label">Status: <span class="star">*</span></label>
                                <select class="form-control" name="status">
                                    <option value="" selected>Status</option>
                                    <option value="1" <?php echo (isset($productData['ti_status']) && $productData['ti_status'] == 1)   ? "selected" : ""; ?>>Active</option>
                                    <option value="0" <?php echo (isset($productData['ti_status']) && $productData['ti_status'] == 0) ? "selected" : ""; ?>>Deactive</option>
                                </select>
                                <span class="error"><?php echo $statusErr; ?> </span>
                            </div>

                            <button name="submit" type="submit" class="btn btn-lg btn-info btn-block">
                                <span name="submit">Submit</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require('footer.inc.php');
?>