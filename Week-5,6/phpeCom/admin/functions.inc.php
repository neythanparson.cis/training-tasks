<?php
function get_safe_value($con,$str){
	if($str!=''){
		//will avoid adding blank spaces
		$str=trim($str);
		// The real_escape_string() / mysqli_real_escape_string() function escapes special characters in a string for use in an SQL query, taking into account the current character set of the connection.
		return mysqli_real_escape_string($con,$str);
	}
}

?>