    //preview image(Category section)
    var preview = function(event) {
        var output = document.getElementById('fileList');
        output.src = URL.createObjectURL(event.target.files[0]);
        // Set the image's source to a new object URL representing the file, using URL.createObjectURL() to create the blob URL.
    };

    //preview multiple images(product section)
    function preview_image(event) {
        var total_file = document.getElementById("upload_file").files.length;
        for (var i = 0; i < total_file; i++) {

            $('#image_preview').append("<img style='width: 100px; height: 90px; border: 2px solid #dddddd; margin:5px; padding:10px'  src='" + URL.createObjectURL(event.target.files[i]) + "'>");
            // Set the image's source to a new object URL representing the file, using URL.createObjectURL() to create the blob URL.
        }
    };

    $(document).ready(function() {
        $('#mytable').DataTable({ searching: false, paging: false, info: false });
    });