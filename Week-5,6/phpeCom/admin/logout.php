<?php
session_start();

// The unset() function unsets a variable.
unset($_SESSION['ADMIN_LOGIN']);
unset($_SESSION['ADMIN_USERNAME']);
//will redirect to the login page 
header('location:login.php');  
die();
?>