<?php
$title = "Add Categoriees";
require('top.inc.php');

$image = $categories = $order = $productnumber = $status = '';
$imageErr = $categoriesErr = $orderErr  = $statusErr = '';
 $msg = '';
//function for validation
function input_data($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

if (isset($_POST['submit'])) {

  //get data from form to table and database
  $categories = get_safe_value($con, $_POST['categories']);
  $order = get_safe_value($con, $_POST['order']);
  $status = get_safe_value($con, $_POST['status']);

  $image = $_FILES['image']['name'];
  $allowed_types = ['jpg', 'png', 'jpeg', 'gif'];

  //name validate
  if (empty($_POST["categories"])) {
    $categoriesErr = "Name is required.";
  }

  //Orders validation
  if (empty($_POST["order"])) {
    $orderErr = "Please enter orders.";
  } else {
    $order = input_data($_POST["order"]);
    // check if price only contains numbers.  
    if (!preg_match("/^[0-9]*$/", $order)) {
      $orderErr = "Only numeric value is allowed.";
    }
  }

  //status validation
  if (($_POST["status"] == '')) {
    $statusErr = "Status is required.";
  }

  if (!$categoriesErr && !$orderErr && !$statusErr);

  $file_ext = pathinfo($image, PATHINFO_EXTENSION);

  // Check file type is allowed or not
  if (in_array(strtolower($file_ext), $allowed_types)) {

    //move_uploaded_files -> bulit in function moves image to given folder path
    move_uploaded_file($_FILES['image']['tmp_name'], '../media/category/' . $image);

    //to avoid duplication/already existed data
    $result_cat = mysqli_query($con, "select * from tbl_categories where v_categories='$categories'");
    $check = mysqli_num_rows($result_cat);
    if ($check > 0) {
      $msg = "* Can not add same category name!";
    } else {

      mysqli_query($con, "INSERT INTO `tbl_categories`(`v_image`, `v_categories`, `dt_addon_date`, `dt_modified_date`, `i_order`, `i_no_of_products`,`ti_status`) VALUES ('$image','$categories',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'$order','$productnumber','$status')");
      header('location:categories.php');
    }
  } else {
    // If file extension not valid
    $imageErr =  "* file type is not allowed (only 'jpg', 'png', 'jpeg', 'gif' allowed)";
  }
}

?>
<div class="content pb-0">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header"><strong>Add Category</strong></div>
          <form method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="card-body card-block">

              <!-- method-2 <div id="fileList">
                       <p  style= display: hidden;></p>
                      </div> -->

              <div class="image-preview">
                <img id="fileList" class="image-preview__image" />
              </div>
                  <div class="error"><?php echo $msg; ?></div>
              <div class="form-group">
                <label for="categories" class=" form-control-label">Category Image: <span class="star">*</span></label>
                <!-- populate data in textbox -->
                <input type="file" name="image" class="form-control" id="fileElem" accept="image/*" onchange="preview(event)">
                <span class="error"><?php echo $imageErr; ?> </span>
              </div>

              <div class="form-group">
                <label for="categories" class=" form-control-label">Category Name: <span class="star">*</span></label>
                <!-- populate data in textbox -->
                <input type="text" name="categories" placeholder="Enter Category" class="form-control">
                <span class="error"><?php echo $categoriesErr; ?> </span>
              </div>

              <div class="form-group">
                <label for="categories" class=" form-control-label">Order: <span class="star">*</span></label>
                <!-- populate data in textbox -->
                <input type="text" name="order" placeholder="Enter Order" class="form-control">
                <span class="error"><?php echo $orderErr; ?> </span>
              </div>

              <div class="form-group">
                <label for="categories" class=" form-control-label">Status: <span class="star">*</span></label>
                <select class="form-control" name="status">
                  <option value="" selected>Status</option>
                  <option value="1">Active</option>
                  <option value="0">Deactive</option>
                </select>
                <span class="error"><?php echo $statusErr; ?> </span>
              </div>

              <button name="submit" type="submit" class="btn btn-lg btn-info btn-block">
                <span name="submit">Submit</span>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
require('footer.inc.php');
?>