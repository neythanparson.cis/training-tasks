<?php
$title = "Products";
require('top.inc.php');

$minPrice = '';
$maxPrice = '';

$minQty = '';
$maxQty = '';

$msg = "";
$str = "";

//STATUS (ACTIVE/DEACTIVE)
if (isset($_GET['type']) && $_GET['type'] != '') {
	$type = get_safe_value($con, $_GET['type']);
	if ($type == 'ti_status') {
		$operation = get_safe_value($con, $_GET['operation']);
		$id = get_safe_value($con, $_GET['id']);
		if ($operation == 'active') {
			$status = '1';
		} else {
			$status = '0';
		}
		$update_status = "UPDATE tbl_product SET ti_status='$status' WHERE id='$id'";
		mysqli_query($con, $update_status);
	}
}

//DELETE 
if (isset($_GET['type']) &&  $_GET['type'] != '') {
	//$GET['type'] :- will return delete means is status has some value or not NULL
	$type = get_safe_value($con, $_GET['type']);
	if ($type == 'delete') {
		$id = get_safe_value($con, $_GET['id']);
		$delete_sql = "DELETE FROM tbl_product WHERE id='$id'";

		mysqli_query($con, $delete_sql);
	}
}



//Search Filter

$res = "SELECT tbl_product.id,tbl_product.v_name,tbl_product.i_product_code,tbl_product.f_price,tbl_product.f_sale_price,tbl_product.i_qty,tbl_product.dt_added_on,tbl_product.dt_modified_on,tbl_product.i_order,tbl_product.ti_status,tbl_categories.v_categories,tbl_product_img.v_image FROM tbl_product LEFT JOIN tbl_categories ON tbl_product.v_categories=tbl_categories.id LEFT JOIN tbl_product_img ON tbl_product.id=tbl_product_img.i_product_id WHERE tbl_product_img.i_main_image='1' ";
// ORDER BY `id` DESC
if (isset($_POST['search'])) {

	$str = get_safe_value($con, $_POST['product-search']);
	$minPrice = $_POST['min-price'];
	$maxPrice = $_POST['max-price'];

	$minQty = $_POST['min-qty'];
	$maxQty = $_POST['max-qty'];

	$cstatus = $_POST['status'];

	// 1	// 1-1-1-1
	if (!empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && !empty($_POST['status'])) {
		$res .= "AND tbl_product.v_name LIKE '%$str%' 
		AND tbl_product.f_price BETWEEN $minPrice AND $maxPrice 
		AND tbl_product.i_qty BETWEEN $minQty AND $maxQty  
		AND tbl_product.ti_status= $cstatus";
	}
	// 2	// 1-0-0-0
	elseif (!empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && empty($_POST['min-qty']) && empty($_POST['max-qty']) && empty($_POST['status'])) {

		$res .= "AND tbl_product.v_name LIKE '%$str%' ";
	}

	// 3	// 1-1-0-0
	elseif (!empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && empty($_POST['min-qty']) && empty($_POST['max-qty']) && empty($_POST['status'])) {

		$res .= "AND tbl_product.v_name LIKE '%$str%' 
		AND tbl_product.f_price BETWEEN $minPrice AND $maxPrice";
	}

	// 4	// 1-1-1-0
	elseif (!empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && empty($_POST['status'])) {

		$res .= "AND tbl_product.v_name LIKE '%$str%' 
		AND tbl_product.f_price BETWEEN $minPrice AND $maxPrice 
		AND tbl_product.i_qty BETWEEN $minQty AND $maxQty ";
	}

	// 5 	// 1-0-1-0
	elseif (!empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && empty($_POST['status'])) {

		$res .= "AND tbl_product.v_name LIKE '%$str%' 
		AND tbl_product.i_qty BETWEEN $minQty AND $maxQty ";
	}

	// 6	// 1-0-1-1
	elseif (!empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && !empty($_POST['status'])) {

		$res .= "AND tbl_product.v_name LIKE '%$str%' 
		AND tbl_product.i_qty BETWEEN $minQty AND $maxQty  
		AND tbl_product.ti_status= $cstatus";
	}

	// 7	// 1-0-0-1
	elseif (!empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && empty($_POST['min-qty']) && empty($_POST['max-qty']) && !empty($_POST['status'])) {

		$res .= "AND tbl_product.v_name LIKE '%$str%' 
		AND tbl_product.ti_status= $cstatus";
	}

	// 8	// 0-1-1-1
	elseif (empty($_POST['product-search']) && empty($_POST['min-price']) && !empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && !empty($_POST['status'])) {

		$res .= "AND tbl_product.f_price BETWEEN $minPrice AND $maxPrice 
		AND tbl_product.i_qty BETWEEN $minQty AND $maxQty  
		AND tbl_product.ti_status= $cstatus";
	}

	// 9	// 0-1-1-0
	elseif (empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && empty($_POST['status'])) {

		$res .= "AND tbl_product.f_price BETWEEN $minPrice AND $maxPrice 
		AND tbl_product.i_qty BETWEEN $minQty AND $maxQty";
	}

	// 10	// 0-1-0-0
	elseif (empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && empty($_POST['min-qty']) && empty($_POST['max-qty']) && empty($_POST['status'])) {

		$res .= "AND tbl_product.f_price BETWEEN $minPrice AND $maxPrice";
	}

	// 11	// 0-1-0-1
	elseif (empty($_POST['product-search']) && !empty($_POST['min-price']) && !empty($_POST['max-price']) && empty($_POST['min-qty']) && empty($_POST['max-qty']) && !empty($_POST['status'])) {

		$res .= "AND tbl_product.f_price BETWEEN $minPrice AND $maxPrice
		AND tbl_product.ti_status= $cstatus";
	}

	// 12	// 0-0-1-0
	elseif (empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && !empty($_POST['min-qty']) && !empty($_POST['max-qty']) && empty($_POST['status'])) {

		$res .= "AND tbl_product.i_qty BETWEEN $minQty AND $maxQty";
	}

	// 13	//0-0-0-1
	elseif (empty($_POST['product-search']) && empty($_POST['min-price']) && empty($_POST['max-price']) && empty($_POST['min-qty']) && empty($_POST['max-qty']) && !empty($_POST['status'])) {

		$res .= "AND tbl_product.ti_status= $cstatus";
	}
}

$result = mysqli_query($con, $res);


?>

<form action="" method="post">
	<div class="content pb-0">
		<div class="orders">
			<div class="row ">
				<div class="col-xl-12 grid-margin stretch-card">
					<form id="change">
							<div class="row">
									<a href="manage_product.php" type="button" class=" btn btn-primary ml-3 "><i class="fa fa-plus" aria-hidden="true">&nbsp;&nbsp;</i> Add New Record!</a>

									<input type="text" name="product-search" placeholder="Product Name" class="form-control col-2 ml-5" value="<?php echo $str ?>">

									<label for=""class="ml-2 mt-2">Price-Range:</label>
									<input type="text" name="min-price" placeholder="Min-price" class="form-control col-1 ml-1" value="<?php echo $minPrice  ?>">
									<input type="text" name="max-price" placeholder="Max-price" class="form-control col-1" value="<?php echo $maxPrice ?>">
	
									<label for=""class="ml-2 mt-2">Qty-Range:</label>
									<input type="text" name="min-qty" placeholder="Min-qty" class="form-control col-1 ml-1" value="<?php echo $minQty ?>">
									<input type="text" name="max-qty" placeholder="Max-qty" class="form-control col-1" value="<?php echo $maxQty ?>">

									<select class="form-control col-1 ml-2" id="status" name="status">
										<option value="" selected>Status</option>
										<option value="'1'" <?php echo (isset($_POST['status']) && $_POST['status'] == "'1'") ? 'selected' : '' ?>>Active</option>
										<option value="'0'" <?php echo (isset($_POST['status']) && $_POST['status'] == "'0'") ? 'selected' : '' ?>>Deactive</option>
									</select>

									<input type="submit" value="Search" name="search" class="btn btn-primary ml-2 "></input>
									<input type="submit" id="reset" value="Reset" name="reset" class="btn btn-danger ml-2 "></input>
									
							</div>
					</form>
					<div class="field_error"><?php echo $msg ?></div>

					<!-- </form> -->

					<div class="card ">
						<div class="card-body">
							<h4 class="box-title mb-3">Product</h4>
							<div class="card-body--">
								<div class="table-stats order-table ov-h">
									<table id="mytable" class="table ">
										<thead>
											<tr>
												<th class="serial">#</th>
												<th>ID</th>
												<th>Image</th>
												<th>Name</th>
												<th>Product Code</th>
												<th>Category name</th>
												<th>Price</th>
												<th>Sale_price</th>
												<th>Qty</th>
												<th>Added Date</th>
												<th>Modified Date/Time</th>
												<th>Order</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$i = 1;
											if ($result->num_rows > 0) {
												while ($row = $result->fetch_assoc()) { ?>
													<tr>
														<td><?php echo $i++ ?></td>
														<td><?php echo $row['id'] ?></td>
														<td><img src="../media/product/<?php echo $row['v_image'] ?>" /></td>
														<td><?php echo $row['v_name'] ?></td>
														<td><?php echo $row['i_product_code'] ?></td>
														<td><?php echo $row['v_categories'] ?></td>
														<td><?php echo $row['f_price'] ?></td>
														<td><?php echo $row['f_sale_price'] ?></td>
														<td><?php echo $row['i_qty'] ?></td>
														<td><?php echo $row['dt_added_on'] ?></td>
														<td><?php echo $row['dt_modified_on'] ?></td>
														<td><?php echo $row['i_order'] ?></td>
														<td>
															<?php
															if ($row['ti_status'] == 1) {
																echo "<a class='btn btn-success' href='?type=ti_status&operation=deactive&id=" . $row['id'] . "'>Active</a>";
															} else {
																echo "<a class='btn btn-warning' href='?type=ti_status&operation=active&id=" . $row['id'] . "'>Deactive</a>";
															}
															?>
														</td>
														<td>
															<?php
															echo "<a class='btn btn-primary' href='manage_product.php?id=" . $row['id'] . "'>Edit</a>&nbsp;";
															echo "<a class='btn btn-danger' href='?type=delete&id=" . $row['id'] . "'>Delete</a>";
															?>
														</td>
													</tr>
											<?php
												}
											}
											?>
										</tbody>

									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<?php require('footer.inc.php') ?>