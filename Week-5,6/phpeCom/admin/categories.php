<?php
$title = "Categories";
require('top.inc.php');

$msg = "";
$str = "";
//STATUS (ACTIVE/DEACTIVE)
if (isset($_GET['type']) && $_GET['type'] != '') {
	//$GET['type'] :- will return ti_status means is status has some value or not NULL
	$type = get_safe_value($con, $_GET['type']);
	if ($type == 'ti_status') {
		$operation = get_safe_value($con, $_GET['operation']);
		$id = get_safe_value($con, $_GET['id']);
		if ($operation == 'active') {
			$status = '1';
		} else {
			$status = '0';
		}
		$update_status = "update tbl_categories set ti_status='$status' where id='$id'";
		mysqli_query($con, $update_status);
	}
}
//DELETE 
if (isset($_GET['type']) &&  $_GET['type'] != '') {
	//$GET['type'] :- will return delete means is status has some value or not NULL
	$type = get_safe_value($con, $_GET['type']);
	if ($type == 'delete') {
		$id = get_safe_value($con, $_GET['id']);
		$delete_sql = "delete from tbl_categories where id='$id'";

		mysqli_query($con, $delete_sql);
	}
}

//Search FILTER
$group = "GROUP BY tbl_categories.id";
$main = "SELECT tbl_categories.id,tbl_categories.v_categories,tbl_categories.v_image,tbl_categories.i_order,tbl_categories.ti_status,tbl_categories.dt_addon_date,tbl_categories.dt_modified_date,COUNT(tbl_product.v_categories) as total FROM tbl_product RIGHT JOIN tbl_categories ON tbl_product.v_categories=tbl_categories.id";
$res = "$main $group";
// echo $res;
// exit;
if (isset($_POST['search'])) {
	$str = get_safe_value($con, $_POST['categoy-search']);
	$cstatus = $_POST['status'];

	//if category search and status both fields are not empty
	if (!empty($_POST['categoy-search']) && !empty($_POST['status'])) {
		$res = " $main WHERE tbl_categories.v_categories LIKE '%$str%' AND tbl_categories.ti_status=$cstatus $group";
		// query will check both fields
	}
	//if category field is empty and status field has some value
	else if (empty($_POST['categoy-search']) && !empty($_POST['status'])) {
		$res =  "$main WHERE tbl_categories.ti_status =$cstatus $group";
		// query will check status ,what value is selected

	}
	//if category field is not empty but status field is empty 
	else if (!empty($_POST['categoy-search']) && empty($_POST['status'])) {
		$res = "$main WHERE tbl_categories.v_categories  LIKE '%$str%' $group";
		// query will check category field only 
	}
}
$result = mysqli_query($con, $res);


?>
<form action="" method="post">
	<div class="content pb-0">
		<div class="orders">
			<div class="row ">
				<div class="col-xl-12 grid-margin stretch-card">
					<form action="" id="change">
						<!-- <form class="form-inline "> -->
						<a href="manage_category.php" type="button" class=" btn btn-primary  mb-3"><i class="fa fa-plus" aria-hidden="true">&nbsp;&nbsp;</i> Add New Record!</a>
						<div class="float-right form-inline ">
							<input type="text" name="categoy-search" placeholder="Category Name" id="categoy-search" class="form-control mb-3" value="<?php echo $str ?>">

							<select class="form-control  ml-2 mb-3" name="status" id="status">
								<option value="" selected>Status</option>
								<option value="'1'" <?php echo (isset($_POST['status']) && $_POST['status'] == "'1'") ? 'selected' : '' ?>>Active</option>
								<!-- 1.This line checks if if var. is declared or not, but works only if the form has a name attribute (name="status").
								 2.&& operator :- checks if both conditions are true thn it will be true
								 3.ternary operator :- it will check  $_POST['status']=="'1'") first,if its true it will retuen 'selected' else ''.
								-->
								<option value="'0'" <?php echo (isset($_POST['status']) && $_POST['status'] == "'0'") ? 'selected' : '' ?>>Deactive</option>
							</select>

							<input type="submit" value="Search" name="search" class="btn btn-primary ml-2 mb-3"></input>
							<!-- Reset form -->
							<input type="submit" value="Reset" name="reset" id="reset" class="btn btn-danger ml-2 mb-3"></input>

						</div>
					</form>
					<div class="field_error"><?php echo $msg ?></div>

					<!-- </form> -->

					<div class="card ">
						<div class="card-body">
							<h4 class="box-title mb-3">Categories </h4>
							<div class="card-body--">
								<div class="table-stats order-table ov-h">
									<table class="table ">
										<thead>
											<tr>
												<th class="serial">#</th>
												<th>ID</th>
												<th>Image</th>
												<th>Categories</th>
												<th>Added Date</th>
												<th>Modified Date/Time</th>
												<th>Order</th>
												<th>NO. of Products</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$i = 1;
											//  The fetch_assoc() / mysqli_fetch_assoc() function fetches a result row as an associative array.
											if ($result->num_rows > 0) {
												while ($row = $result->fetch_assoc()) { ?>
													<tr>
														<td><?php echo $i++ ?></td>
														<td><?php echo $row['id'] ?></td>
														<td><img src="../media/category/<?php echo $row['v_image'] ?>" /></td>
														<td><?php echo $row['v_categories'] ?></td>
														<td><?php echo $row['dt_addon_date'] ?></td>
														<td><?php echo $row['dt_modified_date'] ?></td>
														<td><?php echo $row['i_order'] ?></td>
														<td><?php echo $row['total'] ?></td>
														<td>
															<?php
															if ($row['ti_status'] == 1) {
																echo "<a class='btn btn-success' href='?type=ti_status&operation=deactive&id=" . $row['id'] . "'>Active</a>";
															} else {
																echo "<a class='btn btn-warning' href='?type=ti_status&operation=active&id=" . $row['id'] . "'>Deactive</a>";
															}
															?>
														</td>
														<td>
															<?php
															echo "<a class='btn btn-primary' href='manage_category.php?id=" . $row['id'] . "'>Edit</a>&nbsp;";
															echo "<a class='btn btn-danger' href='?type=delete&id=" . $row['id'] . "'>Delete</a>";
															?>
														</td>
													</tr>
											<?php
												}
											}
											?>
										</tbody>

									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<?php
require('footer.inc.php');
?>