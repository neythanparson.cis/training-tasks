<?php
$title = "Add Products";
require('top.inc.php');


$categories = $productname = $productcode = $price = $saleprice = $qty = $order = $status = "";
$imageErr = $categoriesErr = $productnameErr  = $priceErr = $salepriceErr = $qtyErr = $orderErr = $statusErr = "";

//function for validation
function input_data($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if (isset($_POST['submit'])) {
    //get data from form to table and database
    $categories = get_safe_value($con, $_POST['category']);
    $productname = get_safe_value($con, $_POST['productname']);
    $productcode = mt_rand(1, 10000);
    $price = get_safe_value($con, $_POST['price']);
    $saleprice = get_safe_value($con, $_POST['saleprice']);
    $qty = get_safe_value($con, $_POST['qty']);
    $order = get_safe_value($con, $_POST['order']);
    $status = get_safe_value($con, $_POST['status']);

    $image = $_FILES['image']['name'];
    $allowed_types = ['jpg', 'png', 'jpeg', 'gif'];


    //validation
    //category validation
    if (($_POST["category"] == 0)) {
        $categoriesErr = "Category is required.";
    }

    //name validate
    if (empty($_POST["productname"])) {
        $productnameErr = "Name is required.";
    }

    //price validation
    if (empty($_POST["price"])) {
        $priceErr = "Please enter price.";
    } else {
        $price = input_data($_POST["price"]);
        // check if price only contains numbers.  
        if (!preg_match("/^[0-9]*$/", $price)) {
            $priceErr = "Only numeric value is allowed.";
        }
    }

    //sale price validation
    if (empty($_POST["saleprice"])) {
        $salepriceErr = "Please enter saleprice.";
    } else {
        $saleprice = input_data($_POST["saleprice"]);
        // check if price only contains numbers.  
        if (!preg_match("/^[0-9]*$/", $saleprice)) {
            $salepriceErr = "Only numeric value is allowed.";
        }
    }

    //Qty validation
    if (empty($_POST["qty"])) {
        $qtyErr = "Please enter Quantity.";
    } else {
        $qty = input_data($_POST["qty"]);
        // check if price only contains numbers.  
        if (!preg_match("/^[0-9]*$/", $qty)) {
            $qtyErr = "Only numeric value is allowed.";
        }
    }

    //Orders validation
    if (empty($_POST["order"])) {
        $orderErr = "Please enter orders.";
    } else {
        $order = input_data($_POST["order"]);
        // check if price only contains numbers.  
        if (!preg_match("/^[0-9]*$/", $order)) {
            $orderErr = "Only numeric value is allowed.";
        }
    }

    if (($_POST["status"] == '')) {
        $statusErr = "Status is required.";
    }

    if (!$categoriesErr && !$productnameErr  && !$priceErr && !$salepriceErr && !$qtyErr && !$orderErr && !$statusErr);

    $productQuery = "INSERT INTO`tbl_product`( `v_name`, `v_categories`, `i_product_code`, `f_price`, `f_sale_price`, `i_qty`, `dt_added_on`, `dt_modified_on`, `i_order`, `ti_status`) VALUES ('$productname','$categories','$productcode','$price','$saleprice','$qty',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'$order','$status')";
    if (mysqli_query($con, $productQuery)) {
        $last_id = mysqli_insert_id($con);
    }
    // Count total files
    $countfiles = count($image);
    // print_r($countfiles);
    // exit;

    // Looping all files
    for ($i = 0; $i < $countfiles; $i++) {

        $filename = $_FILES['image']['name'][$i];
        $file_ext = pathinfo($filename, PATHINFO_EXTENSION);

        // Check file type is allowed or not
        if (in_array(strtolower($file_ext), $allowed_types)) {

            // Upload file
            move_uploaded_file($_FILES['image']['tmp_name'][$i], '../media/product/' . $filename);
            if ($i == 0) {
                $imageUploads = "INSERT INTO `tbl_product_img` (`i_product_id`, `v_image`,`i_main_image`) VALUES ('$last_id','$filename','1')";
            } else {
                $imageUploads = "INSERT INTO`tbl_product_img`(`i_product_id`,`v_image`) VALUES  ('$last_id','$filename')";
            }
            $result = mysqli_query($con, $imageUploads);
            header('location:product.php');
        } else {
            // If file extension not valid
            $imageErr =  "* file type is not allowed (only 'jpg', 'png', 'jpeg', 'gif' allowed)";
        }
    }
}

?>
<div class="content pb-0">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><strong>Add Product</strong></div>
                    <form method="post" enctype="multipart/form-data" autocomplete="off">
                        <div class="card-body card-block">

                            <div id="image_preview"></div>
                            
                            <div class="form-group">
                                <label for="product" class=" form-control-label">Categories: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <select name="category" class="form-control">
                                    <option value="0">Select Categories</option>
                                    <?php
                                    $res = mysqli_query($con, "select id,v_categories from tbl_categories where ti_status = 1 order by v_categories asc");
                                    while ($row = mysqli_fetch_assoc($res)) {
                                        echo "<option value=" . $row['id'] . ">" . $row['v_categories'] . "</option>";
                                    }
                                    ?>
                                </select>
                                <span class="error"><?php echo $categoriesErr; ?> </span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Product Image:</label>
                                <!-- populate data in textbox -->
                                <input type="file" name="image[]" class="form-control" id="upload_file" accept="image/*" multiple onchange="preview_image(event)">
                                <span class="field_error"><?php echo $imageErr ?></span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Product Name: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="productname" placeholder="Enter Product Name" class="form-control">
                                <span class="error"><?php echo $productnameErr; ?> </span>
                            </div>


                            <div class="form-group">
                                <label for="product" class=" form-control-label">Price: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="price" placeholder="Enter Product Price" class="form-control">
                                <span class="error"><?php echo $priceErr; ?> </span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Sale Price: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="saleprice" placeholder="Enter Product Sale Price" class="form-control">
                                <span class="error"><?php echo $salepriceErr; ?> </span>
                            </div>

                            <div class="form-group">
                                <label for="product" class=" form-control-label">Quantity: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="qty" placeholder="Enter Quantity" class="form-control">
                                <span class="error"><?php echo $qtyErr; ?> </span>
                            </div>


                            <div class="form-group">
                                <label for="product" class=" form-control-label">Order: <span class="star">*</span></label>
                                <!-- populate data in textbox -->
                                <input type="text" name="order" placeholder="Enter Order" class="form-control">
                                <span class="error"><?php echo $orderErr; ?> </span>
                            </div>


                            <div class="form-group">
                                <label for="product" class=" form-control-label">Status: <span class="star">*</span></label>
                                <select class="form-control" name="status">
                                    <option value="" selected>Status</option>
                                    <option value="1">Active</option>
                                    <option value="0">Deactive</option>
                                </select>
                                <span class="error"><?php echo $statusErr; ?> </span>
                            </div>

                            <button name="submit" type="submit" class="btn btn-lg btn-info btn-block">
                                <span name="submit">Submit</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require('footer.inc.php');
