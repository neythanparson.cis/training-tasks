<?php
if (isset($_GET['id']) && $_GET['id'] != '') {
   $title = "Edit Categories";
} else {
   $title = "Add Categories";
}

require('top.inc.php');

$image = $categories = $productnumber = $order = $status = '';
$imageErr = $categoriesErr = $orderErr = $statusErr = '';


// =>   //Fetch data from table to form
if (isset($_GET['id']) && $_GET['id'] != '') {
   $id = get_safe_value($con, $_GET['id']);
   $res = mysqli_query($con, "select * from tbl_categories where id='$id'");
   $check = mysqli_num_rows($res);
   if ($check > 0) {
      // The fetch_assoc() / mysqli_fetch_assoc() function fetches a result row as an associative array.eg: key => value
      $row = mysqli_fetch_assoc($res);
      // $status = $row['ti_status'];
   } else {
      header('location:categories.php');
      die();
   }
}

// =>   //Submit Edited data
if (isset($_POST['submit'])) {
   $categories = get_safe_value($con, $_POST['categories']);
   $order = get_safe_value($con, $_POST['order']);
   $status = get_safe_value($con, $_POST['status']);

   $image = $_FILES['image']['name'];
   move_uploaded_file($_FILES['image']['tmp_name'], '../media/category/' . $image);
   $file_ext = explode('.', $image);
   $file_ext_check = strtolower(end($file_ext));
   $allowed_types = ['jpg', 'png', 'jpeg', 'gif'];

   if (!isset($_GET['id'])) {
      if (empty($_FILES['image']['name'])) {
         $imageErr = "Please Select Image";
      }
      if (!empty($_FILES['image']['name'])) {
         if (!in_array($file_ext_check, $allowed_types)) {
            $imageErr = "file type is not allowed (only 'jpg', 'png', 'jpeg', 'gif' allowed)";
         }
      }
   } else {
      if (!empty($_FILES['image']['name'])) {
         if (!in_array($file_ext_check, $allowed_types)) {
            $imageErr = "file type is not allowed (only 'jpg', 'png', 'jpeg', 'gif' allowed)";
         }
      }
   }

   // =>   //to avoid duplication/already existed data
   $result_cat = mysqli_query($con, "select * from tbl_categories where v_categories='$categories'");
   $check = mysqli_num_rows($result_cat);
   if ($check > 0) {
      if (isset($_GET['id']) && $_GET['id'] != '') {
         $getdata = mysqli_fetch_assoc($result_cat);
         //while attempting edit fun. it will check that the id of element is same or not if it is same it will not execute .
         if ($id == $getdata['id']) {
         } else {
            $categoriesErr = "Can not add same name!";
         }
      } else {
         $categoriesErr = "Can not add same name!";
      }
   }

   // =>   // validation of form
   //function for validation
   function input_data($data)
   {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
   }

   //name validate
   if (empty($_POST["categories"])) {
      $categoriesErr = "Name is required.";
   }

   //Orders validation
   if (empty($_POST["order"])) {
      $orderErr = "Please enter orders.";
   } else {
      $order = input_data($_POST["order"]);
      // check if price only contains numbers.  
      if (!preg_match("/^[0-9]*$/", $order)) {
         $orderErr = "Only numeric value is allowed.";
      }
   }

   //status validation
   if (($_POST["status"] == '')) {
      $statusErr = "Status is required.";
   }


   if (!$imageErr && !$categoriesErr && !$orderErr && !$statusErr) {

      if (isset($_GET['id']) && $_GET['id'] != '') {

         mysqli_query($con, "UPDATE  tbl_categories SET id=$id, v_categories='$categories',i_order='$order',i_no_of_products='$productnumber',ti_status='$status' WHERE id= '$id'");

         if ($_FILES['image']['error'] == 0) {
            mysqli_query($con, "UPDATE `tbl_categories` SET id=$id,v_image='$image' WHERE id= '$id'");
            // exit;
         }
      } else {
         mysqli_query($con, "INSERT INTO `tbl_categories`(`v_image`, `v_categories`, `dt_addon_date`, `dt_modified_date`, `i_order`, `i_no_of_products`,`ti_status`) VALUES ('$image','$categories',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'$order','$productnumber','$status')");
      }
      header('location:categories.php');
   }
}

?>
<div class="content pb-0">
   <div class="animated fadeIn">
      <div class="row">
         <div class="col-lg-12">
            <div class="card">
               <?php
               if (isset($_GET['id']) && $_GET['id'] != '') { ?>
                  <div class="card-header"><strong>Edit Category</strong></div>
               <?php } else { ?>
                  <div class="card-header"><strong>Add Category</strong></div>
               <?php } ?>


               <form method="post" enctype="multipart/form-data" autocomplete="off">
                  <div class="card-body card-block">
                     <div class="image-preview">

                        <img src="../media/category/<?php echo isset($row['v_image']) ? $row['v_image'] : ''  ?>" id="fileList" class="image-preview__image" />
                        <!-- to get old image when editing form  -->
                     </div>

                     <div class="form-group">
                        <label for="categories" class=" form-control-label">Category Image: <span class="star">*</span></label>
                        <!-- populate data in textbox -->
                        <input type="file" name="image" class="form-control" id="fileElem" onchange="preview(event)">
                        <span class="error"><?php echo $imageErr; ?> </span>
                     </div>

                     <div class="form-group">
                        <label for="categories" class=" form-control-label">Category Name: <span class="star">*</span></label>
                        <!-- populate data in textbox -->
                        <input type="text" name="categories" placeholder="Enter Category" class="form-control" value="<?php echo isset($row['v_categories']) ? $row['v_categories'] : ''  ?>">
                        <span class="error"><?php echo $categoriesErr; ?> </span>
                     </div>

                     <div class="form-group">
                        <label for="categories" class=" form-control-label">Order: <span class="star">*</span></label>
                        <!-- populate data in textbox -->
                        <input type="text" name="order" placeholder="Enter Order" class="form-control" value="<?php echo isset($row['i_order']) ? $row['i_order'] : ''  ?>">
                        <span class="error"><?php echo $orderErr; ?> </span>
                     </div>

                     <div class="form-group">
                        <label for="categories" class=" form-control-label">Status: <span class="star">*</span></label>
                        <select class="form-control" name="status" value="<?php echo $status ?>">
                           <option value="" selected>Status</option>
                           <option value="1" <?php echo (isset($row['ti_status']) && $row['ti_status'] == 1)   ? "selected" : ""; ?>>Active</option>
                           <option value="0" <?php echo (isset($row['ti_status']) && $row['ti_status'] == 0) ? "selected" : ""; ?>>Deactive</option>
                        </select>
                        <span class="error"><?php echo $statusErr; ?> </span>
                     </div>
                     <button name="submit" type="submit" class="btn btn-lg btn-info btn-block">
                        <span name="submit">Submit</span>
                     </button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<?php
require('footer.inc.php');
?>